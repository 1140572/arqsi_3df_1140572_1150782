import React from 'react';
import ReactDOM from 'react-dom';
import Inicio from './Inicio.js';
import DisplayGestor from './DisplayGestor.js';
import DisplayUser from './DisplayUser.js';

class Index extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeDisplayTitle: "Inicio",
            activeDisplay: <Inicio/>
        }
    }

    handleNavbarClick(clickedBtn) {
        let state;

        switch (clickedBtn) {
            case "Inicio":
                state = {
                    activeDisplayTitle: "Inicio",
                    activeDisplay: <Inicio/>
                }
                break;
            case "Produtos":
                state = {
                    activeDisplayTitle: "Produtos",
                    activeDisplay:
                        <div className="content">
                            <DisplayGestor/>
                        </div>
                }
                break;
            case "Encomendas":
                state = {
                    activeDisplayTitle: "Encomendas",
                    activeDisplay:
                        <div className="content">
                            <DisplayUser/>
                        </div>
                }
                break;
            default:
                return;
        }

        this.setState(state);
    }

    render() {
        const MenuItem = (props) => {
            if (props.texto === this.state.activeDisplayTitle) {
                return (
                    <a className='item active'
                       onClick={() => this.handleNavbarClick(props.texto)}>{props.texto}</a>
                );
            } else {
                return (
                    <a className='item'
                       onClick={() => this.handleNavbarClick(props.texto)}>{props.texto}</a>
                );
            }

        }
        return (
            <React.Fragment>
                <div id="navbar" className='ui menu'>
                    <div className='ui medium header item'>Armarios Por Medida</div>
                    <MenuItem texto="Inicio"/>
                    <MenuItem texto="Produtos"/>
                    <MenuItem texto="Encomendas"/>
                    <div className='right menu'>
                        <a className='item'>Login</a>
                        <a className='item'>Registar</a>
                    </div>
                </div>

                {this.state.activeDisplay}

            </React.Fragment>
        );
    }

};

ReactDOM.render(<Index/>, document.querySelector("#root"));