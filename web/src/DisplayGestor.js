import React from 'react';
import {Grid, Menu} from 'semantic-ui-react';
import VerProdutos from "./panels/VerProdutos";
import VerCategorias from "./panels/VerCategorias";
import VerDimensoes from "./panels/VerDimensoes";
import VerMateriais from "./panels/VerMateriais";
import CriarProduto from "./panels/CriarProduto";
import CriarDimensao from "./panels/CriarDimensao";

class DisplayGestor extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeItem: 'produtos',
            activePainel: <VerProdutos/>
        }
    }

    handleItemClick = (e, {name, painel}) => {
        let state = {
            activeItem: name,
            activePainel: painel
        }

        this.setState(state);
    }

    render() {
        const {activeItem} = this.state

        return (
            <Grid id="displayProdutos" className="fade-in">
                <Grid.Row>
                    <Grid.Column className="sidebar" width={3}>
                        <Menu vertical>
                            <Menu.Item
                                painel={<VerProdutos/>}
                                name='produtos'
                                active={activeItem === 'produtos'}
                                onClick={this.handleItemClick}>
                                Produtos
                            </Menu.Item>
                            <Menu.Item
                                painel={<CriarProduto/>}
                                name="CriarProduto"
                                onClick={this.handleItemClick}>
                                Novo Produto
                            </Menu.Item>
                            <Menu.Item
                                painel={<VerDimensoes/>}
                                name='dimensoes'
                                active={activeItem === 'dimensoes'}
                                onClick={this.handleItemClick}>
                                Dimensoes
                            </Menu.Item>
                            <Menu.Item
                                painel={<CriarDimensao/>}
                                name="CriarDimensao"
                                onClick={this.handleItemClick}>
                                Nova Dimensão
                            </Menu.Item>
                            <Menu.Item
                                painel={<VerCategorias/>}
                                name='categorias'
                                active={activeItem === 'categorias'}
                                onClick={this.handleItemClick}>
                                Categorias
                            </Menu.Item>
                            <Menu.Item
                                painel={<VerMateriais/>}
                                name='materiais'
                                active={activeItem === 'materiais'}
                                onClick={this.handleItemClick}>
                                Materiais
                            </Menu.Item>
                        </Menu>
                    </Grid.Column>
                    <Grid.Column className="painel" width={13}>
                        {this.state.activePainel}
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        );
    }
}

export default DisplayGestor;