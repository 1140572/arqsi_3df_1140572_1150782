import React from 'react';
import {Dropdown} from 'semantic-ui-react';
import TableDimensaoContinua from "../components/TableDimensaoContinua";
import TableDimensaoDiscreta from "../components/TableDimensaoDiscreta";

const tiposDimensao = [
    {
        text: 'Continua',
        value: 'Continua'
    },
    {
        text: 'Discreta',
        value: 'Discreta'
    }
]

class CriarDimensao extends React.Component {

    constructor() {
        super();

        this.state = {
            activeTable: <TableDimensaoContinua/>,
            tipoDimensao: "Continua"
        };
    }

    handleDropdownChange = (e, data) => {
        let state;
        console.log(data);

        if (data.value === "Continua") {
            state = {
                activeTable: <TableDimensaoContinua/>,
                tiposDimensao: "Continua"
            }
        } else {
            state = {
                activeTable: <TableDimensaoDiscreta/>,
                tiposDimensao: "Discreta"
            }
        }

        this.setState(state);
    }

    render() {
        return (
            <div className="fade-in">
                <h1>Nova Dimensão</h1>

                <Dropdown defaultValue={"Continua"}
                          onChange={this.handleDropdownChange}
                          selection options={tiposDimensao}></Dropdown>

                {this.state.activeTable}
            </div>
        );
    }
}

export default CriarDimensao;