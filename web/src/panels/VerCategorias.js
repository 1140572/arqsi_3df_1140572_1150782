import React from 'react';
import {List, Grid, Button} from 'semantic-ui-react';
import axios from "axios";

const requestURL = "https://armarios-medida.azurewebsites.net/api/Categoria";

class VerCategorias extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categorias: []
        }
    }

    // TODO
    handleAddCatBtn = () => {
        // Get input from form

        // POST request

        // Check if request returned ok

        // update state
    }

    handleRemoveCatBtn = (catID) => {
        axios.delete(requestURL + "/" + catID)
            .then((response) => { // Success
                console.log(response.data);
                this.setState(
                    {categorias:
                            this.state.categorias.filter(function (categoria) {
                                return categoria.id !== catID;
                            })
                    });
            })
            .catch(function (error) { // Error
                alert(error);
            });
    }

    componentWillMount() {
        axios.get(requestURL)
            .then((response) => { // Success
                console.log(response.data);
                this.setState({categorias: response.data});
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    render() {
        return (
            <div className="fade-in">
                <List verticalAlign="middle" divided relaxed>
                    {this.state.categorias.map(categoria =>
                        <List.Item verticalAlign="middle">
                            <Grid verticalAlign="middle">
                                <Grid.Column floated='left' width={5}>
                                    {categoria.nome}
                                </Grid.Column>
                                <Grid.Column floated='right' width={5}>
                                    <div className='ui two buttons'>
                                        <Button
                                            onClick={() => this.handleRemoveCatBtn(categoria.id)}
                                            basic color='red'>
                                            Apagar
                                        </Button>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </List.Item>
                    )}
                </List>
            </div>
        );
    }
}

export default VerCategorias;