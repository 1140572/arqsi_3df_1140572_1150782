import React from 'react';
import {Button, Grid, List, Dropdown} from "semantic-ui-react";

class VerMateriais extends React.Component {

    componentWillMount() {
        //TODO API

        this.data = [
            {
                nome: "Material0",
                acabamentos: [{
                    nome: "Acabamento0"
                },
                    {
                        nome: "Acabamento1"
                    }
                ]
            },
            {
                nome: "Material1",
                acabamentos: [{
                    nome: "Acabamento0"
                },
                    {
                        nome: "Acabamento2"
                    },
                    {
                        nome: "Acabamento3"
                    },
                    {
                        nome: "Acabamento4"
                    }
                ]
            },
            {
                nome: "Material2",
                acabamentos: [{
                    nome: "Acabamento0"
                },
                    {
                        nome: "Acabamento1"
                    },
                ]
            },
        ];
    }

    render() {
        return (
            <div className="fade-in">
                <Button fluid positive>Adicionar Material</Button>
                <List verticalAlign="middle" divided relaxed>
                    {this.data.map(material =>
                        <List.Item verticalAlign="middle">
                            <Grid verticalAlign="middle">
                                <Grid.Column floated='left' width={5}>
                                    {material.nome}
                                </Grid.Column>
                                <Grid.Column floated='left' width={5}>
                                    <Dropdown placeholder="Acabamentos" fluid selection
                                              options={material.acabamentos.map(acabamento => {
                                                  return { text: acabamento.nome};
                                              })}
                                    />
                                </Grid.Column>
                                <Grid.Column floated='right' width={5}>
                                    <div className='ui two buttons'>
                                        <Button basic color='green'>
                                            Editar
                                        </Button>
                                        <Button basic color='red'>
                                            Apagar
                                        </Button>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </List.Item>
                    )}
                </List>
            </div>
        );
    }
}

export default VerMateriais;