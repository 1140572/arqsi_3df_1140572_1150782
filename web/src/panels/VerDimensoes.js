import React from 'react';
import {Table, Button, Icon} from 'semantic-ui-react';
import axios from "axios";

const requestURL = "https://armarios-medida.azurewebsites.net/api/Dimensao";

class VerDimensoes extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            dimensoes: []
        }
    }

    componentDidMount() {
        this._isMounted = true;

        // GET Request
        axios.get(requestURL)
            .then((response) => { // Success
                if (this._isMounted) {
                    let dimensoes = response.data;

                    if (dimensoes === "") {
                        return;
                    }

                    this.setState({dimensoes: response.data});
                }
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleDelete = (id) => {
        axios.delete(requestURL + "/" + id)
            .then(() => { // Success
                this.setState(
                    {
                        dimensoes:
                            this.state.dimensoes.filter(function (dimensao) {
                                return dimensao.id !== id;
                            })
                    });
            })
            .catch(function (error) { // Error
                alert(error);
            });
    }

    render() {

        let discretas = this.state.dimensoes.filter(dimensao => {
            return (!dimensao.alturaMinima);
        });

        let continuas = this.state.dimensoes.filter(dimensao => {
            return dimensao.alturaMinima;
        });

        return (
            <div className="fade-in">
                <h1>Dimensões</h1>

                <h3>Contínuas</h3>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Altura</Table.HeaderCell>
                            <Table.HeaderCell>Largura</Table.HeaderCell>
                            <Table.HeaderCell>Profundidade</Table.HeaderCell>
                            <Table.HeaderCell collapsing>Apagar</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {continuas.map(dimensao =>
                            <Table.Row key={dimensao.id}>
                                <Table.Cell>{dimensao.id}</Table.Cell>
                                <Table.Cell>
                                    {dimensao.alturaMinima} - {dimensao.alturaMaxima}
                                </Table.Cell>
                                <Table.Cell>
                                    {dimensao.larguraMinima} - {dimensao.larguraMaxima}
                                </Table.Cell>
                                <Table.Cell>
                                    {dimensao.profundidadeMinima} - {dimensao.profundidadeMaxima}
                                </Table.Cell>
                                <Table.Cell collapsing>
                                    <Button icon basic color="red" onClick={() => this.handleDelete(dimensao.id)}>
                                        <Icon name="remove circle" />
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        )}
                    </Table.Body>
                </Table>

                <h3>Discretas</h3>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Altura</Table.HeaderCell>
                            <Table.HeaderCell>Largura</Table.HeaderCell>
                            <Table.HeaderCell>Profundidade</Table.HeaderCell>
                            <Table.HeaderCell collapsing>Apagar</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {discretas.map(dimensao =>
                            <Table.Row key={dimensao.id}>
                                <Table.Cell>{dimensao.id}</Table.Cell>
                                <Table.Cell>{dimensao.altura}</Table.Cell>
                                <Table.Cell>{dimensao.largura}</Table.Cell>
                                <Table.Cell>{dimensao.profundidade}</Table.Cell>
                                <Table.Cell collapsing>
                                    <Button icon basic color="red" onClick={() => this.handleDelete(dimensao.id)}>
                                        <Icon name="remove circle" />
                                    </Button>
                                </Table.Cell>
                            </Table.Row>
                        )}
                    </Table.Body>

                </Table>
            </div>
        );
    }
}

export default VerDimensoes;