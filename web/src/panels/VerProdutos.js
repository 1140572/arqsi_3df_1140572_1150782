import React from 'react';
import {Card} from 'semantic-ui-react';
import axios from 'axios';
import CartaProduto from "../components/CartaProduto";
import '../styles.css';

const requestURL = "https://armarios-medida.azurewebsites.net/api/Produto";

class VerProdutos extends React.Component {
    _isMounted = false;

    constructor() {
        super();

        this.state = {
            produtos: []
        }
    }

    handleDeleteProduto = (idProduto) => {
        axios.delete(requestURL + "/" + idProduto)
            .then(() => { // Success
                this.setState(
                    {
                        produtos:
                            this.state.produtos.filter(function (produto) {
                                return produto.id !== idProduto;
                            })
                    });
            })
            .catch(function (error) { // Error
                alert(error);
            });
    }

    componentDidMount() {
        this._isMounted = true;
        axios.get(requestURL)
            .then((response) => { // Success
                if (this._isMounted && response.data !== "") {
                    this.setState({produtos: response.data});
                }
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return (
            <div>
                <h1>Produtos</h1>
                <Card.Group className="fade-in" stackable={true} doubling={true} itemsPerRow={3}>
                    {this.state.produtos.map(
                        produto =>
                            <CartaProduto
                                onDeleteClick={this.handleDeleteProduto}
                                produto={produto}
                                key={produto.id}/>

                    )}
                </Card.Group>
            </div>

        );
    }
}

export default VerProdutos;