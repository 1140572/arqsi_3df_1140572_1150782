import React from 'react';
import {Input, Dropdown, Segment, Button} from 'semantic-ui-react';
import axios from "axios";

const requestURL = "https://armarios-medida.azurewebsites.net/api/";

class CriarProduto extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            opcoesCategoria: [],
            opcoesProdutos: [],
            opcoesDimensoes: [],
            nomeProduto: "",
            precoProduto: 0,
            categoriaProduto: -1,
            partes: [],
            dimensoes: [],
            materiais: []
        }
    }

    componentDidMount() {
        //GET Categorias
        axios.get(requestURL + "Categoria")
            .then((response) => { // Success
                if (response.data === "") return;
                let opcoesCategoria = [];

                response.data.forEach(function (categoria) {
                    opcoesCategoria.push({
                        key: categoria.id,
                        value: categoria.id,
                        text: categoria.nome
                    })
                });
                this.setState({opcoesCategoria: opcoesCategoria});
            })
            .catch(function (error) { // Error
                console.log(error);
            });

        // GET Produtos
        axios.get(requestURL + "Produto")
            .then((response) => { // Success
                if (response.data === "") return;
                let opcoesProdutos = [];

                response.data.forEach(function (produto) {
                    opcoesProdutos.push({
                        key: produto.id,
                        value: produto.id,
                        text: produto.nome
                    })
                });

                this.setState({opcoesProdutos: opcoesProdutos});
            })
            .catch(function (error) { // Error
                console.log(error);
            });

        //GET Dimensoes
        axios.get(requestURL + "Dimensao")
            .then((response) => { // Success
                if (response.data === "") return;
                let opcoesDimensoes = [];

                response.data.forEach(function (dimensao) {
                    let text = "";

                    if (dimensao.alturaMinima) {
                        text =
                            "Altura: " + dimensao.alturaMinima + "-" + dimensao.alturaMaxima
                            + " | Largura: " + dimensao.larguraMinima + "-" + dimensao.larguraMaxima
                            + " | Profundidade: " + dimensao.profundidadeMinima + "-" + dimensao.profundidadeMaxima
                    } else {
                        text =
                            "Altura: " + dimensao.altura
                            + " | Largura: " + dimensao.largura
                            + " | Profundidade: " + dimensao.profundidade

                    }

                    opcoesDimensoes.push({
                        key: dimensao.id,
                        value: dimensao.id,
                        text: text
                    })
                });

                this.setState({opcoesDimensoes: opcoesDimensoes});
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    handleNomeChange = (e) => {
        this.setState({
            nomeProduto: e.target.value
        })
    }

    handlePrecoChange = (e) => {
        this.setState({
            precoProduto: e.target.value
        })
    }

    handleCategoriaChange = (e, data) => {
        this.setState({
            categoriaProduto: data.value
        })
    }

    handlePartesChange = (e, data) => {
        this.setState({
            partes: data.value
        });
    }

    handleDimensoesChange = (e, data) => {
        this.setState({
            dimensoes: data.value
        });
    }

    handleSubmit = () => {
        let dimensoes = [];

        this.state.dimensoes.forEach(function (dimensaoId) {

            axios.get(requestURL + "Dimensao/" + dimensaoId)
                .then((response) => { // Success
                    let dimensaoEncontrada = response.data;

                    if (dimensaoEncontrada.alturaMinima) {
                        console.log("Dimensao Continua")
                        // Continua
                        dimensoes.push({
                            type: "Continua",
                            alturaMinima: dimensaoEncontrada.alturaMinima,
                            alturaMaxima: dimensaoEncontrada.alturaMaxima,
                            larguraMinima: dimensaoEncontrada.larguraMinima,
                            larguraMaxima: dimensaoEncontrada.larguraMaxima,
                            profundidadeMinima: dimensaoEncontrada.profundidadeMinima,
                            profundidadeMaxima: dimensaoEncontrada.profundidadeMaxima,
                        });
                    } else {
                        // Discreta
                        dimensoes.push({
                            type: "Discreta",
                            altura: dimensaoEncontrada.altura,
                            largura: dimensaoEncontrada.largura,
                            profundidade: dimensaoEncontrada.altura
                        });
                    }

                })
                .catch(function (error) { // Error
                    console.log(error);
                });
        });

        console.log("Dimensoes", dimensoes);

        // POST Request
        axios.post(requestURL + "Produto", {
            Nome: this.state.nomeProduto,
            Preco: this.state.precoProduto,
            IdCategoria: this.state.categoriaProduto,
            Partes: this.state.partes,
            dimensoes: dimensoes
        })
            .then((response) => { // Success
                console.log("Criado com sucesso");
                console.log(response.data);
            })
            .catch(function (error) { // Error
                console.log(error);
            });


    }

    render() {
        return (
            <div className="fade-in">
                <h1>Criar Produto</h1>
                <Segment>
                    <h2>Informações Básicas</h2>
                    <p>Informações básicas do produto</p>
                    <Input onChange={this.handleNomeChange} label='Nome Produto' value={this.state.nomeProduto}/>
                    <Input onChange={this.handlePrecoChange} label='Preço' value={this.state.precoProduto}/>
                    <Dropdown onChange={this.handleCategoriaChange} placeholder='Categoria' fluid search selection
                              options={this.state.opcoesCategoria}/>
                </Segment>
                <Segment>
                    <h2>Agregação</h2>
                    <p>Adicionar outros produtos ao atual</p>
                    <Dropdown onChange={this.handlePartesChange} placeholder='Partes' fluid multiple selection
                              options={this.state.opcoesProdutos}/>
                </Segment>
                <Segment>
                    <h2>Dimensões</h2>
                    <p>Selecione um conjunto de dimensões para o produto</p>
                    <Dropdown onChange={this.handleDimensoesChange} placeholder='Dimensões' fluid multiple selection
                              options={this.state.opcoesDimensoes}/>
                </Segment>
                <Button onClick={this.handleSubmit} fluid primary>Concluir</Button>
            </div>
        );
    }
}

export default CriarProduto;