import React from 'react';
import './landing.css';

class Inicio extends React.Component {

    componentWillMount() {
        document.querySelector('body').style.background =
            "linear-gradient(to right, #2F80ED, #56CCF2)"
        document.querySelector('body').style.background =
            "-webkit-linear-gradient(to right, #2F80ED, #56CCF2)"
    }

    componentWillUnmount() {
        document.querySelector('body').style.background = "white";
        document.querySelector('body').style.color = "black";
    }

    render() {
        return (
            <div className="ui vertical masthead center aligned segment meio">
                <div className="ui text container">
                    <h1 id="title" className="ui huge inverted header">
                        Armários <br/><span id="small-text">Por  </span>Medida
                    </h1>
                    <h2>Configuração e Venda de Armários personalizados.</h2>
                    <div>
                        <div id="btnEsquerda" className="ui huge primary button">
                            Configurar Produtos
                            <i className="right arrow icon"></i>
                        </div>
                        <div id="btnDireita" className="ui huge primary button">
                            Encomendar Armário
                            <i className="right arrow icon"></i>
                        </div>
                    </div>

                </div>

            </div>
        );
    }
}

export default Inicio;