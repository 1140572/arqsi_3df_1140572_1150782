import React from 'react';
import {Table, Input, Button} from 'semantic-ui-react';
import axios from "axios";

const requestURL = "https://armarios-medida.azurewebsites.net/api/Dimensao";

class TableDimensaoDiscreta extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            altura: null,
            largura: null,
            profundidade: null
        }
    }

    handleClick = () => {
        let altura = this.state.altura;
        let largura = this.state.largura;
        let profundidade = this.state.profundidade;

        // Validaçao dados
        if (isNaN(altura) || isNaN(largura) || isNaN(profundidade)) {
            alert("Valor tem de ser numerico!");
            return;
        }

        // POST Request
        axios.post(requestURL, {
            type: "Discreta",
            altura: altura,
            largura: largura,
            profundidade: profundidade
        })
            .then((response) => { // Success
                console.log(response.data);
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    handleAlturaChange = (e, data) => {
        this.setState({altura: data.value});
    }

    handleLarguraChange = (e, data) => {
        this.setState({largura: data.value});
    }

    handleProfundidadeChange = (e, data) => {
        this.setState({profundidade: data.value});
    }

    render() {
        return (
            <div className="fade-in">
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Lado</Table.HeaderCell>
                            <Table.HeaderCell>Valor</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>Altura</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleAlturaChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Largura</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleLarguraChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Profundidade</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleProfundidadeChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>

                <Button onClick={this.handleClick} fluid primary>Confirmar</Button>
            </div>
        );
    }
}

export default TableDimensaoDiscreta;