import React from 'react';
import {Card, Button} from 'semantic-ui-react';

class Carta extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card>
                <Card.Content>
                    <Card.Header>{this.props.header}</Card.Header>
                    <Card.Meta>{this.props.meta}</Card.Meta>
                    {this.props.content}
                </Card.Content>
                <Card.Content className='ui two buttons'>
                    <Button onClick={this.props.edit} basic primary>
                        Editar
                    </Button>
                    <Button onClick={this.props.onDeleteClick} basic color='red'>
                        Apagar
                    </Button>
                </Card.Content>
            </Card>
        );
    }
}

export default Carta;