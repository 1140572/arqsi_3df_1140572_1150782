import React from 'react';
import {Table, Input, Button} from 'semantic-ui-react';
import axios from "axios";

const requestURL = "https://armarios-medida.azurewebsites.net/api/Dimensao";

class TableDimensaoContinua extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            alturaMin: null,
            alturaMax: null,
            larguraMin: null,
            larguraMax: null,
            profundidadeMin: null,
            profundidadeMax: null
        }
    }

    handleClick = () => {
        let alturaMin = this.state.alturaMin;
        let alturaMax = this.state.alturaMax;
        let larguraMin = this.state.larguraMin;
        let larguraMax = this.state.larguraMax;
        let profundidadeMin = this.state.profundidadeMin;
        let profundidadeMax = this.state.profundidadeMax;

        // Validaçao dados
        if (isNaN(alturaMin) || isNaN(alturaMax)
            || isNaN(larguraMin) || isNaN(larguraMax)
            || isNaN(profundidadeMin) || isNaN(profundidadeMax)
        ) {
            alert("Valor tem de ser numerico!");
            return;
        }

        // POST Request
        axios.post(requestURL, {
            type: "Continua",
            alturaMinima: alturaMin,
            alturaMaxima: alturaMax,
            larguraMinima: larguraMin,
            larguraMaxima: alturaMax,
            profundidadeMinima: profundidadeMin,
            profundidadeMaxima: profundidadeMax
        })
            .then((response) => { // Success
                console.log(response.data);
            })
            .catch(function (error) { // Error
                console.log(error);
            });
    }

    handleAlturaMinChange = (e, data) => {
        this.setState({alturaMin: data.value});
    }

    handleAlturaMaxChange = (e, data) => {
        this.setState({alturaMax: data.value});
    }

    handleLarguraMinChange = (e, data) => {
        this.setState({larguraMin: data.value});
    }

    handleLarguraMaxChange = (e, data) => {
        this.setState({larguraMax: data.value});
    }

    handleProfundidadeMinChange = (e, data) => {
        this.setState({profundidadeMin: data.value});
    }

    handleProfundidadeMaxChange = (e, data) => {
        this.setState({profundidadeMax: data.value});
    }

    render() {
        return (
            <div className="fade-in">
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Lado</Table.HeaderCell>
                            <Table.HeaderCell>Minimo</Table.HeaderCell>
                            <Table.HeaderCell>Máximo</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>Altura</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleAlturaMinChange} placeholder="Valor"></Input>
                            </Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleAlturaMaxChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Largura</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleLarguraMinChange} placeholder="Valor"></Input>
                            </Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleLarguraMaxChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Profundidade</Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleProfundidadeMinChange} placeholder="Valor"></Input>
                            </Table.Cell>
                            <Table.Cell>
                                <Input onChange={this.handleProfundidadeMaxChange} placeholder="Valor"></Input>
                            </Table.Cell>
                        </Table.Row>

                    </Table.Body>
                </Table>

                <Button onClick={this.handleClick} fluid primary>Confirmar</Button>
            </div>
        );
    }
}

export default TableDimensaoContinua;