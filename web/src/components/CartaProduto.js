import React from 'react';
import {Button, Card} from "semantic-ui-react";


class CartaProduto extends React.Component {

    constructor(props) {
        super(props);

        let conteudo = (
            <div className="contentCarta">
                <ul>
                    <li><b>Preço: </b>{props.produto.preco}</li>

                    <li><b>Categoria: </b> {props.produto.categoria.nome}</li>

                    <li>
                        <b>Partes: </b>
                        <ul>
                            {props.produto.partes.map(parte => <li><b>ID: </b>{parte.id}</li>)}
                        </ul>
                    </li>

                    <li>
                        <b>Dimensoes: </b>
                        <ul>
                            {props.produto.dimensoes.map(dimensao => <li><b>ID: </b> {dimensao.id}</li>)}
                        </ul>
                    </li>

                    <li>
                        <b>Materiais: </b>
                        <ul>
                            {props.produto.materiais.map(mat => <li><b>ID: </b> {mat.id}</li>)}
                        </ul>
                    </li>
                </ul>

            </div>
        );

        this.state = {
            produto: props.produto,
            content: conteudo
        }
    }

    render() {

        return (
            <Card className="fade-in">
                <Card.Content>
                    <Card.Header>{this.state.produto.nome}</Card.Header>
                    <Card.Meta>{this.state.produto.id}</Card.Meta>
                    {this.state.content}
                </Card.Content>
                <Card.Content className='ui two buttons'>
                    <Button onClick={this.props.edit} basic primary>
                        Editar
                    </Button>
                    <Button onClick={() => this.props.onDeleteClick(this.state.produto.id)}
                            basic color='red'>
                        Apagar
                    </Button>
                </Card.Content>
            </Card>
        );
    }
}

export default CartaProduto;