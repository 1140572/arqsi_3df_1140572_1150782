﻿// <auto-generated />
using System;
using ArmariosPorMedida;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ArmariosPorMedida.Migrations
{
    [DbContext(typeof(PersistenceContext))]
    [Migration("20181019192955_MigrationAfterReset")]
    partial class MigrationAfterReset
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ArmariosPorMedida.Model.Acabamento", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao");

                    b.Property<long?>("MaterialId");

                    b.HasKey("Id");

                    b.HasIndex("MaterialId");

                    b.ToTable("Acabamentos");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Categoria", b =>
                {
                    b.Property<long>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("nome");

                    b.Property<long?>("superCatid");

                    b.HasKey("id");

                    b.HasIndex("superCatid");

                    b.ToTable("Categorias");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Dimensao", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("Altura");

                    b.Property<double>("Largura");

                    b.Property<double>("Profundidade");

                    b.HasKey("Id");

                    b.ToTable("Dimensoes");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Material", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao");

                    b.HasKey("Id");

                    b.ToTable("Materiais");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Produto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long?>("Categoriaid");

                    b.Property<long?>("DimensaoId");

                    b.Property<long?>("MaterialId");

                    b.Property<string>("Nome");

                    b.Property<long?>("ParteEmId");

                    b.Property<double>("Preco");

                    b.HasKey("Id");

                    b.HasIndex("Categoriaid");

                    b.HasIndex("DimensaoId");

                    b.HasIndex("MaterialId");

                    b.HasIndex("ParteEmId");

                    b.ToTable("Produtos");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Acabamento", b =>
                {
                    b.HasOne("ArmariosPorMedida.Model.Material")
                        .WithMany("Acabamentos")
                        .HasForeignKey("MaterialId");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Categoria", b =>
                {
                    b.HasOne("ArmariosPorMedida.Model.Categoria", "superCat")
                        .WithMany("subCats")
                        .HasForeignKey("superCatid");
                });

            modelBuilder.Entity("ArmariosPorMedida.Model.Produto", b =>
                {
                    b.HasOne("ArmariosPorMedida.Model.Categoria", "Categoria")
                        .WithMany()
                        .HasForeignKey("Categoriaid");

                    b.HasOne("ArmariosPorMedida.Model.Dimensao", "Dimensao")
                        .WithMany()
                        .HasForeignKey("DimensaoId");

                    b.HasOne("ArmariosPorMedida.Model.Material", "Material")
                        .WithMany()
                        .HasForeignKey("MaterialId");

                    b.HasOne("ArmariosPorMedida.Model.Produto", "ParteEm")
                        .WithMany("Partes")
                        .HasForeignKey("ParteEmId");
                });
#pragma warning restore 612, 618
        }
    }
}
