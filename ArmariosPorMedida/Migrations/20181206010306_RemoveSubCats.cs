﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArmariosPorMedida.Migrations
{
    public partial class RemoveSubCats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categorias_Categorias_superCatid",
                table: "Categorias");

            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Categorias_Categoriaid",
                table: "Produtos");

            migrationBuilder.RenameColumn(
                name: "Categoriaid",
                table: "Produtos",
                newName: "CategoriaId");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_Categoriaid",
                table: "Produtos",
                newName: "IX_Produtos_CategoriaId");

            migrationBuilder.RenameColumn(
                name: "superCatid",
                table: "Categorias",
                newName: "SuperCatId");

            migrationBuilder.RenameColumn(
                name: "nome",
                table: "Categorias",
                newName: "Nome");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Categorias",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_Categorias_superCatid",
                table: "Categorias",
                newName: "IX_Categorias_SuperCatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Categorias_Categorias_SuperCatId",
                table: "Categorias",
                column: "SuperCatId",
                principalTable: "Categorias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Categorias_CategoriaId",
                table: "Produtos",
                column: "CategoriaId",
                principalTable: "Categorias",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categorias_Categorias_SuperCatId",
                table: "Categorias");

            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Categorias_CategoriaId",
                table: "Produtos");

            migrationBuilder.RenameColumn(
                name: "CategoriaId",
                table: "Produtos",
                newName: "Categoriaid");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_CategoriaId",
                table: "Produtos",
                newName: "IX_Produtos_Categoriaid");

            migrationBuilder.RenameColumn(
                name: "SuperCatId",
                table: "Categorias",
                newName: "superCatid");

            migrationBuilder.RenameColumn(
                name: "Nome",
                table: "Categorias",
                newName: "nome");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Categorias",
                newName: "id");

            migrationBuilder.RenameIndex(
                name: "IX_Categorias_SuperCatId",
                table: "Categorias",
                newName: "IX_Categorias_superCatid");

            migrationBuilder.AddForeignKey(
                name: "FK_Categorias_Categorias_superCatid",
                table: "Categorias",
                column: "superCatid",
                principalTable: "Categorias",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Categorias_Categoriaid",
                table: "Produtos",
                column: "Categoriaid",
                principalTable: "Categorias",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
