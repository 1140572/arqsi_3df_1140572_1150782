﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArmariosPorMedida.Migrations
{
    public partial class Pocos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PObrigatorioId",
                table: "Restricoes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restricoes_PObrigatorioId",
                table: "Restricoes",
                column: "PObrigatorioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restricoes_Produtos_PObrigatorioId",
                table: "Restricoes",
                column: "PObrigatorioId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restricoes_Produtos_PObrigatorioId",
                table: "Restricoes");

            migrationBuilder.DropIndex(
                name: "IX_Restricoes_PObrigatorioId",
                table: "Restricoes");

            migrationBuilder.DropColumn(
                name: "PObrigatorioId",
                table: "Restricoes");
        }
    }
}
