﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ArmariosPorMedida.Migrations
{
    public partial class DimensionsMaterialsChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Dimensoes_DimensaoId",
                table: "Produtos");

            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Materiais_MaterialId",
                table: "Produtos");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_DimensaoId",
                table: "Produtos");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_MaterialId",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "DimensaoId",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "MaterialId",
                table: "Produtos");

            migrationBuilder.AddColumn<long>(
                name: "ProdutoId",
                table: "Materiais",
                nullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "Profundidade",
                table: "Dimensoes",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<float>(
                name: "Largura",
                table: "Dimensoes",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<float>(
                name: "Altura",
                table: "Dimensoes",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<float>(
                name: "AlturaMaxima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "AlturaMinima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "LarguraMaxima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "LarguraMinima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "ProfundidadeMaxima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "ProfundidadeMinima",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Dimensoes",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "ProdutoId",
                table: "Dimensoes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Restricoes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    ProdutoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restricoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Restricoes_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Materiais_ProdutoId",
                table: "Materiais",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_ProdutoId",
                table: "Dimensoes",
                column: "ProdutoId");

            migrationBuilder.CreateIndex(
                name: "IX_Restricoes_ProdutoId",
                table: "Restricoes",
                column: "ProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dimensoes_Produtos_ProdutoId",
                table: "Dimensoes",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dimensoes_Produtos_ProdutoId",
                table: "Dimensoes");

            migrationBuilder.DropForeignKey(
                name: "FK_Materiais_Produtos_ProdutoId",
                table: "Materiais");

            migrationBuilder.DropTable(
                name: "Restricoes");

            migrationBuilder.DropIndex(
                name: "IX_Materiais_ProdutoId",
                table: "Materiais");

            migrationBuilder.DropIndex(
                name: "IX_Dimensoes_ProdutoId",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "Materiais");

            migrationBuilder.DropColumn(
                name: "AlturaMaxima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "AlturaMinima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "LarguraMaxima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "LarguraMinima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "ProfundidadeMaxima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "ProfundidadeMinima",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Dimensoes");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "Dimensoes");

            migrationBuilder.AddColumn<long>(
                name: "DimensaoId",
                table: "Produtos",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MaterialId",
                table: "Produtos",
                nullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Profundidade",
                table: "Dimensoes",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Largura",
                table: "Dimensoes",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Altura",
                table: "Dimensoes",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_DimensaoId",
                table: "Produtos",
                column: "DimensaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_MaterialId",
                table: "Produtos",
                column: "MaterialId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Dimensoes_DimensaoId",
                table: "Produtos",
                column: "DimensaoId",
                principalTable: "Dimensoes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Materiais_MaterialId",
                table: "Produtos",
                column: "MaterialId",
                principalTable: "Materiais",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
