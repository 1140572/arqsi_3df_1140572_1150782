﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArmariosPorMedida.Migrations
{
    public partial class RemovesParteEm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Produtos_ParteEmId",
                table: "Produtos");

            migrationBuilder.RenameColumn(
                name: "ParteEmId",
                table: "Produtos",
                newName: "ProdutoId");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_ParteEmId",
                table: "Produtos",
                newName: "IX_Produtos_ProdutoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Produtos_ProdutoId",
                table: "Produtos",
                column: "ProdutoId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_Produtos_ProdutoId",
                table: "Produtos");

            migrationBuilder.RenameColumn(
                name: "ProdutoId",
                table: "Produtos",
                newName: "ParteEmId");

            migrationBuilder.RenameIndex(
                name: "IX_Produtos_ProdutoId",
                table: "Produtos",
                newName: "IX_Produtos_ParteEmId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_Produtos_ParteEmId",
                table: "Produtos",
                column: "ParteEmId",
                principalTable: "Produtos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
