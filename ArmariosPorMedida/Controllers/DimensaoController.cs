using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArmariosPorMedida.Controllers
{
    [Route("api/Dimensao")]
    [ApiController]
    public class DimensaoController : ControllerBase
    {
        private readonly IDimensaoService _service;

        public DimensaoController(IDimensaoService service)
        {
            _service = service;
        }

        [HttpGet(Name = "GetDimensoes")]
        public ActionResult<List<DimensaoDTO>> GetAll()
        {
            List<DimensaoDTO> dimensaos = _service.GetAllDimensoes();

            if (dimensaos.Count == 0)
            {
                return NoContent();
            }

            return dimensaos;
        }

        [HttpGet("{id:int}", Name = "GetDimensaoById")]
        public ActionResult<DimensaoDTO> GetById(long id)
        {
            DimensaoDTO dim = _service.GetDimensaoById(id);

            if (dim == null) return NotFound();

            return dim;
        }

        [HttpPost]
        public IActionResult Create(DimensaoPOCO dimensao)
        {
            Dimensao criada = _service.AddDimensao(dimensao);
            
            if (criada != null)
            {
                return CreatedAtRoute("GetDimensoes", criada);
            }

            return BadRequest();
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(long id, Dimensao dimensao)
        {
            if (!_service.EditDimensao(id, dimensao))
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            if (!_service.RemoveDimensao(id))
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}