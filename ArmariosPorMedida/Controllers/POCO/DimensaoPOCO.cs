﻿namespace ArmariosPorMedida.Controllers
{
    public class DimensaoPOCO
    {
        public string Type { get; set; }

        public float Altura { get; set; }
        public float Profundidade { get; set; }
        public float Largura { get; set; }

        public float AlturaMinima { get; set; }
        public float AlturaMaxima { get; set; }
        public float ProfundidadeMinima { get; set; }
        public float ProfundidadeMaxima { get; set; }
        public float LarguraMinima { get; set; }
        public float LarguraMaxima { get; set; }

        public DimensaoPOCO()
        {

        }
    }
}
