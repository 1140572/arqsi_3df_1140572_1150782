using System.Collections.Generic;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Controllers.POCO
{
    public class ProdutoPOCO
    {
        public string Nome { get; set; }

        public double Preco { get; set; }

        public long IdCategoria { get; set; }

        public List<long> Partes { get; set; }

        public List<DimensaoPOCO> Dimensoes { get; set; }

        public List<Material> Materiais { get; set; }
    }
}