using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArmariosPorMedida.Controllers
{
    [Route("api/Categoria")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly ICategoriaService _service;

        public CategoriaController(ICategoriaService service) {
            _service = service;
        }

        [HttpGet(Name = "GetCategorias")]
        public ActionResult<List<CategoriaDTO>> GetAll()
        {
            List<CategoriaDTO> categorias = _service.GetAllCategorias();

            if (categorias.Count == 0) {
                return NoContent();
            }

            return categorias;
        }

        [HttpGet("{id:int}", Name = "GetCategoriaById")]
        public ActionResult<CategoriaDTO> GetById(long id) {
            CategoriaDTO c = _service.GetCategoriaById(id);

            if (c == null) return NotFound();

            return c;
        }

        [HttpGet("nome={nome}", Name = "GetCategoriaByNome")]
        public ActionResult<CategoriaDTO> GetByNome(string nome) {
            CategoriaDTO categoriaEncontrada = _service.GetCategoriaByNome(nome);

            if (categoriaEncontrada == null) return NotFound();

            return categoriaEncontrada;
        }

        [HttpPost]
        public IActionResult Create(Categoria categoria) {
            if (!_service.AddCategoria(categoria)) {
                return BadRequest();
            }

            return CreatedAtRoute("GetCategorias", new {id = categoria.Id}, categoria);
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(long id, Categoria categoria) {
            if (!_service.EditCategoria(id, categoria)) {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id) {
            if (!_service.RemoveCategoria(id)) {
                return NotFound();
            }

            return NoContent();
        }
    }
}