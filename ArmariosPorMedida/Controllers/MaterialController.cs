using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArmariosPorMedida.Controllers
{
    [Route("api/Material")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly IMaterialService _service;

        public MaterialController(IMaterialService service) {
            _service = service;
        }

        [HttpGet(Name = "Get")]
        public ActionResult<List<MaterialDTO>> GetAll()
        {
            List<MaterialDTO> materiais = _service.GetAllMateriais();

            if (materiais.Count == 0) {
                return NoContent();
            }

            return materiais;
        }

        [HttpGet("{id:int}", Name = "GetMaterialById")]
        public ActionResult<MaterialDTO> GetById(long id) {
            MaterialDTO ma = _service.GetMaterialById(id);

            if (ma == null) return NotFound();

            return ma;
        }

        [HttpPost]
        public IActionResult Create(Material Material) {
            if (!_service.AddMaterial(Material)) {
                return BadRequest();
            }

            return CreatedAtRoute("Get", new {id = Material.Id}, Material);
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(long id, Material Material) {
            if (!_service.EditMaterial(id, Material)) {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id) {
            if (_service.RemoveMaterial(id)) {
                return NoContent();
            }

            return NotFound();
        }
        
    }
}