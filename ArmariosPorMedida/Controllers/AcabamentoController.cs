using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArmariosPorMedida.Controllers
{
    [Route("api/Acabamento")]
    [ApiController]
    public class AcabamentoController : ControllerBase
    {
        private readonly IAcabamentoService _service;

        public AcabamentoController(IAcabamentoService service)
        {
            _service = service;
        }

        [HttpGet(Name = "GetAcabamentos")]
        public ActionResult<List<AcabamentoDTO>> GetAll()
        {
            List<AcabamentoDTO> acabamentos = _service.GetAllAcabamentos();

            if (acabamentos.Count == 0)
            {
                return NoContent();
            }

            return acabamentos;
        }

        [HttpGet("{id:int}", Name = "GetAcabamentoById")]
        public ActionResult<AcabamentoDTO> GetById(long id)
        {
            AcabamentoDTO dim = _service.GetAcabamentoById(id);

            if (dim == null) return NotFound();

            return dim;
        }

        [HttpPost]
        public IActionResult Create(Acabamento acabamento)
        {
            Acabamento criado = _service.AddAcabamento(acabamento);
            
            if (criado == null)
            {
                return CreatedAtRoute("GetAcabamentos", criado);
            }

            return BadRequest();
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(long id, Acabamento Acabamento)
        {
            if (!_service.EditAcabamento(id, Acabamento))
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            if (!_service.RemoveAcabamento(id))
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}