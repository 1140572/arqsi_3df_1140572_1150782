using System;
using System.Collections.Generic;
using ArmariosPorMedida.Controllers.POCO;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Model.Restrictions;
using ArmariosPorMedida.Services;
using Microsoft.AspNetCore.Mvc;

namespace ArmariosPorMedida.Controllers
{
    [Route("api/Produto")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoService _service;

        public ProdutoController(IProdutoService service)
        {
            _service = service;
        }

        [HttpGet(Name = "GetProdutos")]
        public ActionResult<List<ProdutoDTO>> GetAll()
        {
            List<ProdutoDTO> produtos = _service.GetAll();

            if (produtos.Count == 0)
            {
                return NoContent();
            }

            return produtos;
        }

        [HttpGet("{id:int}", Name = "GetProdutoById")]
        public ActionResult<ProdutoDTO> GetById(long id)
        {
            ProdutoDTO p = _service.GetById(id);

            if (p == null) return NotFound();

            return p;
        }

        [HttpGet("nome={nome}", Name = "GetProdutoByNome")]
        public ActionResult<ProdutoDTO> GetByNome(string nome)
        {
            List<ProdutoDTO> produtos = _service.GetAll();

            foreach (ProdutoDTO p in produtos)
            {
                if (p.Nome == nome)
                {
                    return p;
                }
            }

            return NotFound();
        }

        [HttpPost]
        public IActionResult Create(ProdutoPOCO produtoPoco)
        {
            if (_service.Add(produtoPoco) != null)
            {
                return CreatedAtRoute("GetProdutos", produtoPoco);
            }

            return BadRequest();
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(long id, Produto produto)
        {
            if (!_service.Edit(id, produto))
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpGet("{id:int}/ParteEm", Name = "GetParteEm")]
        public ActionResult<ProdutoDTO> ParteEm(long id)
        {
            // TODO
            return NoContent();
        }

        [HttpGet("{idProduto:int}/Partes", Name = "GetPartes")]
        public ActionResult<List<ProdutoDTO>> Partes(long idProduto)
        {
            List<ProdutoDTO> partes = _service.GetPartes(idProduto);

            if (partes == null) return NotFound();

            return partes;
        }

        [HttpPost("{idProduto:int}/Partes")]
        public IActionResult AddNewParte(int idProduto, [FromBody] int idParte)
        {
            try
            {
                _service.AddNewParte(idProduto, idParte);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e);
            }
            catch (RestricaoException e)
            {
                return BadRequest(e);
            }

            return Accepted();
        }

        [HttpDelete("{idProduto:int}/Partes")]
        public IActionResult DeleteParte(int idProduto, int idParte)
        {
            try
            {
                _service.RemoveParte(idProduto, idParte);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e);
            }

            return Accepted();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            if (!_service.Remove(id))
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}