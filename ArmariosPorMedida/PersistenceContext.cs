using ArmariosPorMedida.Model;
using ArmariosPorMedida.Model.Restrictions;
using Microsoft.EntityFrameworkCore;

namespace ArmariosPorMedida {

    public class PersistenceContext : DbContext {

        public DbSet<Categoria> Categorias {get; set;}

        public DbSet<Dimensao> Dimensoes {get; set;}
        public DbSet<Continua> Continuas {get; set;}
        public DbSet<Discreta> Discretas {get; set;}

        public DbSet<Material> Materiais {get; set;}

        public DbSet<Acabamento> Acabamentos {get; set;}

        public DbSet<Produto> Produtos {get; set;}
        
        public DbSet<Restricao> Restricoes { get; set; }
        public DbSet<Caber> RestricaoCaber { get; set; }
        public DbSet<Ocupacao> RestricaoOcupacao { get; set; }

        public PersistenceContext(DbContextOptions<PersistenceContext> options)
            : base(options)
        {}
    
    }
}