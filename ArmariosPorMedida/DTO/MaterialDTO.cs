using System.Collections.Generic;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.DTO
{
    public class MaterialDTO
    {
        public long Id { get; set; }

        public string Descricao { get; set;}

        public List<Acabamento> Acabamentos {get; set;}

        protected MaterialDTO()
        {

        }

        public MaterialDTO(string descricao, List<Acabamento> acabamentos)
        {
            Descricao = descricao;
            Acabamentos = acabamentos;
        }
    }
   }
