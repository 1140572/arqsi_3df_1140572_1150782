namespace ArmariosPorMedida.DTO
{
    public class AcabamentoDTO
    {
        public long Id { get; set; }

        public string Descricao { get; set; }

        protected AcabamentoDTO()
        {

        }

        public AcabamentoDTO(string descricao)
        {
            Descricao = descricao;
        }
    }
}