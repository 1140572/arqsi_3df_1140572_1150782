namespace ArmariosPorMedida.DTO
{
    public class DiscretaDto : DimensaoDTO
    {
        public double Altura { get; set; }

        public double Largura { get; set; }

        public double Profundidade { get; set; }

        protected DiscretaDto(long id, double altura, double largura, double profundidade) 
            : base(id)
        {
            Altura = altura;
            Largura = largura;
            Profundidade = profundidade;
        }
    }
}