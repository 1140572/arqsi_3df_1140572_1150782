using System.Collections.Generic;

namespace ArmariosPorMedida.DTO
{

    public class ProdutoDTO
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public double Preco { get; set; }

        public List<ProdutoDTO> Partes { get; set; }

        public CategoriaDTO Categoria { get; set; }

        public List<DimensaoDTO> Dimensoes { get; set; }

        public List<MaterialDTO> Materiais { get; set; }

        protected ProdutoDTO()
        {
        }

        public ProdutoDTO(long id, string nome, double preco, CategoriaDTO categoria,
            List<ProdutoDTO> partes, List<DimensaoDTO> dimensoes,
            List<MaterialDTO> materiais)
        {
            Id = id;
            Nome = nome;
            Preco = preco;
            Partes = partes;
            Categoria = categoria;
            Dimensoes = dimensoes;
            Materiais = materiais;
        }
    }
}