namespace ArmariosPorMedida.DTO
{
    public class DimensaoDTO
    {
        public long Id { get; set; }

        protected DimensaoDTO(long id)
        {
            Id = id;
        }
    }
}