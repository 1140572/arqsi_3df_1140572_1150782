namespace ArmariosPorMedida.DTO
{
    public class ContinuaDto : DimensaoDTO
    {
        public float AlturaMinima { get; set; }
        public float AlturaMaxima { get; set; }
        public float ProfundidadeMinima { get; set; }
        public float ProfundidadeMaxima { get; set; }
        public float LarguraMinima { get; set; }
        public float LarguraMaxima { get; set; }

        protected ContinuaDto(long id, float alturaMinima, float alturaMaxima, 
            float profundidadeMinima, float profundidadeMaxima, 
            float larguraMinima, float larguraMaxima)
            : base(id)
        {
            AlturaMinima = alturaMinima;
            AlturaMaxima = alturaMaxima;
            ProfundidadeMinima = profundidadeMinima;
            ProfundidadeMaxima = profundidadeMaxima;
            LarguraMinima = larguraMinima;
            LarguraMaxima = larguraMaxima;
        }
    }
}