using System.Collections.Generic;

namespace ArmariosPorMedida.DTO
{
    public class CategoriaDTO
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public CategoriaDTO SuperCat { get; set; }

        protected CategoriaDTO()
        {

        }

        public CategoriaDTO(long id, string nome, CategoriaDTO superCat)
        {   
            Id = id;
            Nome = nome;
            SuperCat = superCat;
        }
    }
}