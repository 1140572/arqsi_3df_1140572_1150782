using System.Collections.Generic;
using ArmariosPorMedida.Controllers.POCO;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Services
{
    public interface IProdutoService
    {
        List<ProdutoDTO> GetAll();
        ProdutoDTO GetById(long id);
        List<ProdutoDTO> GetPartes(long id);
        void AddNewParte(int idProduto, int newParte);
        Produto Add(ProdutoPOCO produtoPoco);
        bool Edit(long id, Produto alterado);
        bool Remove(long id);
        void RemoveParte(int idProduto, int idParte);
    }
}