using System;
using System.Collections.Generic;
using ArmariosPorMedida.Controllers;
using ArmariosPorMedida.Controllers.POCO;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using AutoMapper;

namespace ArmariosPorMedida.Services
{
    public class ProdutoServiceImpl : IProdutoService
    {
        private readonly IRepository<Produto> _repository;
        private readonly IRepository<Categoria> _catRepository;
        private readonly IDimensaoService _dimService;

        public ProdutoServiceImpl(IRepository<Produto> repository,
            IRepository<Categoria> catRepository, IDimensaoService dimService)
        {
            _repository = repository;
            _catRepository = catRepository;
            _dimService = dimService;
        }

        public List<ProdutoDTO> GetAll()
        {
            List<Produto> todosProdutos = _repository.GetAll();

            return Mapper.Map<List<Produto>, List<ProdutoDTO>>(todosProdutos);
        }

        public ProdutoDTO GetById(long id)
        {
            Produto produto = _repository.GetById(id);

            return Mapper.Map<Produto, ProdutoDTO>(produto);
        }
        public List<ProdutoDTO> GetPartes(long id)
        {
            Produto produto = _repository.GetById(id);

            return Mapper.Map<List<Produto>, List<ProdutoDTO>>(produto.Partes);
        }

        public void AddNewParte(int idProduto, int newParte)
        {
            Produto produto = _repository.GetById(idProduto);
            Produto parte = _repository.GetById(newParte);

            if (produto == null || parte == null)
            {
                throw new ArgumentException("Produto não existe");
            }

            produto.AddParte(parte);

            _repository.Edit(idProduto, produto);
        }

        public void RemoveParte(int idProduto, int idParte)
        {
            Produto produto = _repository.GetById(idProduto);
            Produto parte = _repository.GetById(idParte);
            
            if (produto == null || parte == null)
            {
                throw new ArgumentException("Produto não existe");
            }

            produto.Partes.Remove(parte);

            _repository.Edit(idProduto, produto);
        }

        public Produto Add(ProdutoPOCO produtoPoco)
        {
            if (produtoPoco == null) return null;

            // Get Categoria from ID
            long catId = produtoPoco.IdCategoria;
            Categoria catEncontrada = null;
            if (catId != null)
            {
                catEncontrada = _catRepository.GetById(catId);
            }

            // Create Dimensoes
            List<DimensaoPOCO> dimensoesPoco = produtoPoco.Dimensoes;
            List<Dimensao> dimensoes = new List<Dimensao>();
            if (dimensoesPoco != null)
            {
                foreach (var dimensaoPoco in dimensoesPoco)
                {
                    Dimensao dimensao = _dimService.AddDimensao(dimensaoPoco);
                    dimensoes.Add(dimensao);
                }
            }

            // Get Partes from List of IDs
            List<long> idPartes = produtoPoco.Partes;
            List<Produto> partes = new List<Produto>();
            if (idPartes != null)
            {
                foreach (var idParte in idPartes)
                {
                    Produto parte = _repository.GetById(idParte);
                    partes.Add(parte);
                }
            }
            
            Produto criado = new Produto(
                produtoPoco.Nome, produtoPoco.Preco, catEncontrada,
                partes, dimensoes, produtoPoco.Materiais);

            _repository.Add(criado);
            return criado;
        }

        public bool Edit(long id, Produto alterado)
        {
            _repository.Edit(id, alterado);

            return true;
        }

        public bool Remove(long id)
        {
            Produto produto = _repository.GetById(id);

            if (produto == null)
            {
                return false;
            }

            _repository.Remove(id);
            return true;
        }
    }
}