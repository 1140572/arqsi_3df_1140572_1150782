using System.Collections.Generic;
using ArmariosPorMedida.Controllers;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using AutoMapper;

namespace ArmariosPorMedida.Services
{
    public class DimensaoServiceImpl : IDimensaoService
    {
        private readonly IRepository<Dimensao> _dimensaoRepository;

        public DimensaoServiceImpl(IRepository<Dimensao> dimensaoRepository)
        {
            _dimensaoRepository = dimensaoRepository;
        }

        public List<DimensaoDTO> GetAllDimensoes()
        {
            List<Dimensao> todasDimensoes = _dimensaoRepository.GetAll();
            
            return Mapper.Map<List<Dimensao>, List<DimensaoDTO>>(todasDimensoes);
        }

        public DimensaoDTO GetDimensaoById(long id)
        {
            Dimensao dimensao = _dimensaoRepository.GetById(id);

            return Mapper.Map<Dimensao, DimensaoDTO>(dimensao);
        }

        public Dimensao AddDimensao(DimensaoPOCO dimensaoPoco)
        {
            if (dimensaoPoco.Type == null) return null;

            if (dimensaoPoco.Type.Equals("Discreta")
                || dimensaoPoco.Type.Equals("discreta"))
            {
                Discreta discreta = new Discreta(dimensaoPoco.Altura,
                    dimensaoPoco.Profundidade, dimensaoPoco.Largura);

                _dimensaoRepository.Add(discreta);

                return discreta;
            }

            if (dimensaoPoco.Type.Equals("Continua")
                || dimensaoPoco.Type.Equals("continua"))
            {
                Continua continua = new Continua(
                    dimensaoPoco.AlturaMinima, dimensaoPoco.AlturaMaxima,
                    dimensaoPoco.ProfundidadeMinima, dimensaoPoco.ProfundidadeMaxima,
                    dimensaoPoco.LarguraMinima, dimensaoPoco.LarguraMaxima
                );

                _dimensaoRepository.Add(continua);

                return continua;
            }

            return null;
        }

        public bool EditDimensao(long id, Dimensao alterada)
        {
            return _dimensaoRepository.Edit(id, alterada);
        }

        public bool RemoveDimensao(long id)
        {
            Dimensao dimensao = _dimensaoRepository.GetById(id);

            if (dimensao == null)
            {
                return false;
            }

            _dimensaoRepository.Remove(id);
            return true;
        }



    }
}