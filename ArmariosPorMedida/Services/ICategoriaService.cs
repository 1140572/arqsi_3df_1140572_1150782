using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Services
{
    public interface ICategoriaService
    {
        List<CategoriaDTO> GetAllCategorias();
        CategoriaDTO GetCategoriaById(long id);
        CategoriaDTO GetCategoriaByNome(string nome);
        bool AddCategoria(Categoria categoria);
        bool EditCategoria(long id, Categoria alterada);
        bool RemoveCategoria(long id);
    }
}