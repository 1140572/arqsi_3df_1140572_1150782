using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using AutoMapper;

namespace ArmariosPorMedida.Services
{
    public class MaterialServiceImpl : IMaterialService
    {
        private readonly IRepository<Material> _materialRepository;

        public MaterialServiceImpl(IRepository<Material> materialRepository)
        {
            _materialRepository = materialRepository;
        }

        public List<MaterialDTO> GetAllMateriais()
        {
            List<Material> todosMateriaisMaterial = _materialRepository.GetAll();

            return Mapper.Map<List<Material>, List<MaterialDTO>>(todosMateriaisMaterial);
        }

        public MaterialDTO GetMaterialById(long id)
        {
            Material material = _materialRepository.GetById(id);

            return Mapper.Map<Material, MaterialDTO>(material);

        }

        public bool AddMaterial(Material Material)
        {

            if (Material == null)
            {
                return false;
            }

            _materialRepository.Add(Material);
            return true;
        }

        public bool EditMaterial(long id, Material alterado)
        {
            _materialRepository.Edit(id, alterado);

            return true;
        }

        public bool RemoveMaterial(long id)
        {
            Material Material = _materialRepository.GetById(id);

            return Material != null && _materialRepository.Remove(id);
        }
    }
}