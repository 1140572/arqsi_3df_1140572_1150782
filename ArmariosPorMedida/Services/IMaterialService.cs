using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Services
{
    public interface IMaterialService
    {
        List<MaterialDTO> GetAllMateriais();
        MaterialDTO GetMaterialById(long id);
        bool AddMaterial(Material materialAcabamento);
        bool EditMaterial(long id, Material alterado);
        bool RemoveMaterial(long id);
    }
}