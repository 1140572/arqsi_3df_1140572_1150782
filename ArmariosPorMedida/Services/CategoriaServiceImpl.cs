using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using AutoMapper;

namespace ArmariosPorMedida.Services
{
    public class CategoriaServiceImpl : ICategoriaService
    {
        private readonly IRepository<Categoria> _categoriaRepository;

        public CategoriaServiceImpl(IRepository<Categoria> categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        public List<CategoriaDTO> GetAllCategorias()
        {
            List<Categoria> todasCategorias = _categoriaRepository.GetAll();
            return Mapper.Map<List<Categoria>, List<CategoriaDTO>>(todasCategorias);
        }

        public CategoriaDTO GetCategoriaById(long id)
        {
            Categoria categoria = _categoriaRepository.GetById(id);

            return Mapper.Map<Categoria, CategoriaDTO>(categoria);
        }

        public CategoriaDTO GetCategoriaByNome(string nome)
        {
            foreach (Categoria c in _categoriaRepository.GetAll())
            {
                if (Equals(c.Nome, nome)) {
                    return Mapper.Map<Categoria, CategoriaDTO>(c);
                }
            }

            return null;
        }

        public bool AddCategoria(Categoria categoria)
        {

            if (categoria == null)
            {
                return false;
            }

            _categoriaRepository.Add(categoria);
            return true;
        }

        public bool EditCategoria(long id, Categoria alterada)
        {
            return _categoriaRepository.Edit(id, alterada);
        }

        public bool RemoveCategoria(long id)
        {
            Categoria categoria = _categoriaRepository.GetById(id);

            if (categoria == null)
            {
                return false;
            }

            _categoriaRepository.Remove(id);
            return true;
        }
    }
}