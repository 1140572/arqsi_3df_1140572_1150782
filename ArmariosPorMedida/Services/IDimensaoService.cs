using System.Collections.Generic;
using ArmariosPorMedida.Controllers;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Services
{
    public interface IDimensaoService
    {
        List<DimensaoDTO> GetAllDimensoes();
        DimensaoDTO GetDimensaoById(long id);
        Dimensao AddDimensao(DimensaoPOCO dimensao);
        bool EditDimensao(long id, Dimensao alterada);
        bool RemoveDimensao(long id);
    }
}