using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using AutoMapper;

namespace ArmariosPorMedida.Services
{
    public class AcabamentoServiceImpl : IAcabamentoService
    {
        private readonly IRepository<Acabamento> _acabamentoRepository;

        public AcabamentoServiceImpl(IRepository<Acabamento> acabamentoRepository)
        {
            _acabamentoRepository = acabamentoRepository;
        }

        public List<AcabamentoDTO> GetAllAcabamentos()
        {
            List<Acabamento> todosMateriaisAcabamento = _acabamentoRepository.GetAll();

            return Mapper.Map<List<Acabamento>, List<AcabamentoDTO>>(todosMateriaisAcabamento);
        }

        public AcabamentoDTO GetAcabamentoById(long id)
        {
            Acabamento acabamento = _acabamentoRepository.GetById(id);

            return Mapper.Map<Acabamento, AcabamentoDTO>(acabamento);

        }

        public Acabamento AddAcabamento(Acabamento acabamento)
        {
            if (acabamento == null) return null;
            
             _acabamentoRepository.Add(acabamento);
             return acabamento;
        }

        public bool EditAcabamento(long id, Acabamento alterado)
        {
            _acabamentoRepository.Edit(id, alterado);

            return true;
        }

        public bool RemoveAcabamento(long id)
        {
            Acabamento acabamento = _acabamentoRepository.GetById(id);

            if (acabamento == null)
            {
                return false;
            }

            return _acabamentoRepository.Remove(id);
        }



    }
}