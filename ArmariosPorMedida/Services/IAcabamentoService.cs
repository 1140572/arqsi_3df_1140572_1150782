using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Services
{
    public interface IAcabamentoService
    {
        List<AcabamentoDTO> GetAllAcabamentos();
        AcabamentoDTO GetAcabamentoById(long id);
        Acabamento AddAcabamento(Acabamento materialAcabamento);
        bool EditAcabamento(long id, Acabamento alterado);
        bool RemoveAcabamento(long id);
    }
}