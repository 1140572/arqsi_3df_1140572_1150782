using System;

namespace ArmariosPorMedida.Model.Restrictions
{
    public class RestricaoException : Exception
    {
        public RestricaoException()
        {
        }

        public RestricaoException(string message)
            : base(message)
        {
        }

        public RestricaoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
