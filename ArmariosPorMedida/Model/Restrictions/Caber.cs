using System.Collections.Generic;

namespace ArmariosPorMedida.Model.Restrictions
{
    public class Caber : Restricao
    {
        public override void Run(Produto pai, List<Produto> filhos)
        {
            foreach (Produto filho in filhos)
            {
                bool filhoCabe = false;

                foreach (Dimensao dimensaoPai in pai.Dimensoes)
                {
                    foreach (Dimensao dimensaoFilho in filho.Dimensoes) {

                        if (dimensaoFilho.CabeEm(dimensaoPai))
                        {
                            filhoCabe = true;
                        }
                    }
                }

                if (!filhoCabe)
                {
                    throw new RestricaoException("Produto agregado " + filho.Nome
                                                + " não cabe em produto agregador "
                                                 + pai.Nome);
                }
            }
        }
    }
}