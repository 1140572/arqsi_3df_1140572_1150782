using System.Collections.Generic;

namespace ArmariosPorMedida.Model.Restrictions {

    public abstract class Restricao {
        
        public long Id { get; set; }

        public Restricao()
        {
            
        }
        
        public abstract void Run(Produto pai, List<Produto> filhos);
    }
}