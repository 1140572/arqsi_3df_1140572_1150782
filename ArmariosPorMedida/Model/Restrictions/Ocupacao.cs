using System;
using System.Collections.Generic;

namespace ArmariosPorMedida.Model.Restrictions
{
    public class Ocupacao : Restricao
    {
        public Produto PObrigatorio { get; set; }

        public Ocupacao(Produto pObrigatorio) 
        {
            PObrigatorio = pObrigatorio 
                ?? throw new ArgumentNullException("Produto Obrigatorio cannot be null");
        }
        
        protected Ocupacao() {
        
        }

        public override void Run(Produto pai, List<Produto> filhos)
        {
            if (!pai.Partes.Contains(PObrigatorio)) {
                throw new RestricaoException("Produto Agregador " + pai.Nome
                                            + " tem de agregar Produto + "
                                             + PObrigatorio.Nome);
            }
        }
    }
}