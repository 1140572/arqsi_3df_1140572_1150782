using System;
using System.Collections.Generic;
using System.Linq;
using ArmariosPorMedida.Model.Restrictions;

namespace ArmariosPorMedida.Model
{

    public class Produto
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public double Preco { get; set; }

        public Categoria Categoria { get; set; }

        public List<Produto> Partes { get; set; }

        public List<Dimensao> Dimensoes { get; set; }

        public List<Material> Materiais { get; set; }

        public List<Restricao> Restricoes { get; set; }

        protected Produto()
        {
        }

        public Produto(string nome, double preco, Categoria categoria, 
            List<Dimensao> dimensoes, List<Material> materiais)
        {
            Nome = nome;
            Preco = preco;
            Categoria = categoria;
            Partes = new List<Produto>();
            
            if (dimensoes == null)
            {
                Dimensoes = new List<Dimensao>();
            }
            else
            {
                Dimensoes = dimensoes;
            }
            
            if (materiais == null)
            {
                Materiais = new List<Material>();
            }
            else
            {
                Materiais = materiais;
            }
            
            Restricoes = new List<Restricao>();

            Restricoes.Add(new Caber());
        }

        public Produto(string nome, double preco, Categoria categoria,
            List<Produto> partes, List<Dimensao> dimensao,
            List<Material> materiais)
        {
            Nome = nome;
            Preco = preco;
            Categoria = categoria;
            Partes = partes;
            Dimensoes = dimensao;
            Materiais = materiais;
        }

        public void AddParte(Produto novaParte)
        {
            List<Produto> tempPartes = Partes.ToList();
            tempPartes.Add(novaParte);

            // Pode lançar exceção se nao cumprir
            // com as restriçoes
            VerificaRestricoes(tempPartes);
            
            Partes.Add(novaParte);
        }

        public void VerificaRestricoes(List<Produto> filhos)
        {
            foreach (Restricao restricao in Restricoes)
            {
                restricao.Run(this, filhos);
            }
        }

        public bool RemoveParte(Produto parte)
        {
            return Partes.Remove(parte);
        }

        public override bool Equals(object obj)
        {
            Produto otherObj = obj as Produto;

            if (otherObj == null) return false;

            return Nome == otherObj.Nome;               
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Nome);
        }
    }
}