using System.Collections.Generic;

namespace ArmariosPorMedida.Model
{

    public class Categoria
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public Categoria SuperCat { get; set; }

        protected Categoria()
        {

        }
        
        public Categoria(string nome, Categoria superCat)
        {
            this.Nome = nome;
            this.SuperCat = superCat;
        }
    }
}