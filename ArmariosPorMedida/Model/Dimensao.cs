using System;

namespace ArmariosPorMedida.Model
{
    public abstract class Dimensao
    {
        public long Id { get; set; }
        
        protected Dimensao()
        {
            
        }
        
        public abstract Boolean CabeEm(Dimensao outraDimensao);
    }
}