using System;

namespace ArmariosPorMedida.Model
{
    public class Discreta : Dimensao
    {
        public float Altura { get; set; }
        public float Profundidade { get; set; }
        public float Largura { get; set; }

        public Discreta(float altura, float profundidade, float largura)
        {
            if (altura < 0 || profundidade < 0 || largura < 0)
            {
                throw new ArgumentException("Dimension value must be positive");
            }
            
            Altura = altura;
            Profundidade = profundidade;
            Largura = largura;
        }

        public override bool CabeEm(Dimensao outraDimensao)
        {
            if (outraDimensao.GetType() == typeof(Continua))
            {
                Continua continua = outraDimensao as Continua;

                return Altura <= continua.AlturaMaxima
                    && Altura >= continua.AlturaMinima
                    && Profundidade <= continua.ProfundidadeMaxima
                    && Profundidade >= continua.ProfundidadeMaxima
                    && Largura <= continua.LarguraMaxima
                    && Largura >= continua.LarguraMaxima;
            } 

            if (outraDimensao.GetType() == typeof(Discreta))
            {
                Discreta discreta = outraDimensao as Discreta;

                return Altura < discreta.Altura
                    && Profundidade < discreta.Profundidade
                    && Largura < discreta.Largura;
            }

            throw new ArgumentException("Dimension must be either discrete or range");
        }

        public override bool Equals(object obj)
        {
            Discreta outraDiscreta = obj as Discreta;

            if (outraDiscreta == null)
            {
                return false;
            }

            return Id == outraDiscreta.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}