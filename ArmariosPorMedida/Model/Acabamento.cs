namespace ArmariosPorMedida.Model
{
    public class Acabamento
    {
        public long Id { get; set; }

        public string Descricao { get; set; }

        protected Acabamento()
        {

        }

        public Acabamento(string descricao)
        {
            Descricao = descricao;
        }
    }
}