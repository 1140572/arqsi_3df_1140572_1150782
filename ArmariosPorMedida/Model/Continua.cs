using System;

namespace ArmariosPorMedida.Model
{
    public class Continua : Dimensao
    {
        public float AlturaMinima { get; set; }
        public float AlturaMaxima { get; set; }
        public float ProfundidadeMinima { get; set; }
        public float ProfundidadeMaxima { get; set; }
        public float LarguraMinima { get; set; }
        public float LarguraMaxima { get; set; }

        public Continua(
            float alturaMinima, float alturaMaxima, 
            float profundidadeMinima, float profundidadeMaxima,
            float larguraMinima, float larguraMaxima)
        {
            if (alturaMinima >= alturaMaxima
                || profundidadeMinima >= profundidadeMaxima
                || larguraMinima >= larguraMaxima
                || alturaMinima < 0 
                || profundidadeMinima < 0 
                || larguraMinima < 0)
            {
                throw new ArgumentException("Invalid Dimension Value");
            }

            AlturaMinima = alturaMinima;
            AlturaMaxima = alturaMaxima;
            ProfundidadeMinima = profundidadeMinima;
            ProfundidadeMaxima = profundidadeMaxima;
            LarguraMinima = larguraMinima;
            LarguraMaxima = larguraMaxima;
        }

        public override bool CabeEm(Dimensao outraDimensao)
        {
            if (outraDimensao == null)
            {
                throw new ArgumentNullException("Cant fit inside null Dimension");
            }
            
            // Verifica se esta dimensao está contida na outra
            if (outraDimensao.GetType() == typeof(Continua))
            {
                return AlturaMinima > ((Continua) outraDimensao).AlturaMinima
                    && AlturaMaxima < ((Continua) outraDimensao).AlturaMaxima
                    && ProfundidadeMinima > ((Continua) outraDimensao).ProfundidadeMinima
                    && ProfundidadeMaxima < ((Continua) outraDimensao).ProfundidadeMaxima
                    && LarguraMinima > ((Continua) outraDimensao).LarguraMinima
                    && LarguraMaxima < ((Continua) outraDimensao).LarguraMaxima
                ;
            } 
            
            // Verifica se a outra dimensao está contida no alcance desta
            if (outraDimensao.GetType() == typeof(Discreta))
            {
                return ((Discreta) outraDimensao).Altura > AlturaMinima
                       && ((Discreta) outraDimensao).Altura < AlturaMaxima
                       && ((Discreta) outraDimensao).Profundidade > ProfundidadeMinima
                       && ((Discreta) outraDimensao).Profundidade < ProfundidadeMaxima
                       && ((Discreta) outraDimensao).Largura > LarguraMinima
                       && ((Discreta) outraDimensao).Largura < LarguraMaxima
                    ;
            }

            throw new ArgumentException("Dimension must be Range or Discrete value");
        }
        
        public override bool Equals(object obj)
        {
            Continua outraContinua = obj as Continua;

            if (outraContinua == null)
            {
                return false;
            }

            return Id == outraContinua.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}