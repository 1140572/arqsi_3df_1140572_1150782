using System.Collections.Generic;

namespace ArmariosPorMedida.Model
{
    public class Material
    {
        public long Id { get; set; }

        public string Descricao { get; set;}

        public List<Acabamento> Acabamentos {get; set;}

        protected Material()
        {

        }

        public Material(string descricao, List<Acabamento> acabamentos)
        {
            Descricao = descricao;
            Acabamentos = acabamentos;
        }
    }
}