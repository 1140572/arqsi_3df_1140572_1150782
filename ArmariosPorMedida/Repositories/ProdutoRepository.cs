using System.Collections.Generic;
using System.Linq;
using ArmariosPorMedida.Model;
using Microsoft.EntityFrameworkCore;

namespace ArmariosPorMedida.Repositories
{

    public class ProdutoRepository : Repository<Produto>
    {
        private readonly PersistenceContext _context;

        public ProdutoRepository(PersistenceContext context) :
            base(context, context.Produtos)
        {
            _context = context;
        }

        public override List<Produto> GetAll()
        {
            return _context.Produtos
            .Include(p => p.Categoria)
                .ThenInclude(c => c.SuperCat)
            .Include(p => p.Partes)
            .Include(p => p.Dimensoes)
            .Include(p => p.Materiais)
                    .ThenInclude(m => m.Acabamentos)
            .Include(p => p.Restricoes)
            .ToList();
        }

        public override Produto GetById(long id)
        {
            return _context.Produtos
                .Include(p => p.Categoria)
                    .ThenInclude(c => c.SuperCat)
                .Include(p => p.Partes)
                .Include(p => p.Dimensoes)
                .Include(p => p.Materiais)
                    .ThenInclude(m => m.Acabamentos)
                .Include(p => p.Restricoes)
                .SingleOrDefault(p => p.Id == id);
        }

        public override bool Edit(long id, Produto produtoAlterado)
        {
            Produto produtoEncontrado = _context.Produtos.Find(id);

            if (produtoEncontrado == null)
            {
                return false;
            }

            produtoEncontrado.Nome = produtoAlterado.Nome;
            produtoEncontrado.Preco = produtoAlterado.Preco;
            produtoEncontrado.Categoria = produtoAlterado.Categoria;
            produtoEncontrado.Partes = produtoAlterado.Partes;
            produtoEncontrado.Dimensoes = produtoAlterado.Dimensoes;
            produtoEncontrado.Materiais = produtoAlterado.Materiais;
            produtoEncontrado.Restricoes = produtoAlterado.Restricoes;

            _context.Produtos.Update(produtoEncontrado);
            _context.SaveChanges();

            return true;
        }
    }
}