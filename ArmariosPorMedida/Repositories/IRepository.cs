using System.Collections.Generic;

namespace ArmariosPorMedida.Repositories
{
    public interface IRepository<T>
        where T : class 
    {
        List<T> GetAll();
        T GetById(long id);
        void Add(T t);
        bool Edit(long id, T updated);
        bool Remove(long id);
    }
}