using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ArmariosPorMedida.Repositories
{
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        private readonly PersistenceContext _context;
        
        private readonly DbSet<T> _dbSet;

        protected Repository(PersistenceContext context, DbSet<T> dbSet)
        {
            _context = context;
            _dbSet = dbSet;
        }
        
        public virtual List<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public virtual T GetById(long id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Add(T t)
        {
            _dbSet.Add(t);
            _context.SaveChanges();
        }

        public abstract bool Edit(long id, T updated);

        public virtual bool Remove(long id)
        {
            T t = _dbSet.Find(id);

            if (t == null)
            {
                return false;
            }

            _dbSet.Remove(t);
            _context.SaveChanges();

            return true;
        }
    }
}