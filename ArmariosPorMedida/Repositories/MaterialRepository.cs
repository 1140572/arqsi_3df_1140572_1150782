using System.Collections.Generic;
using System.Linq;
using ArmariosPorMedida.Model;
using Microsoft.EntityFrameworkCore;

namespace ArmariosPorMedida.Repositories
{
    public class MaterialRepository : Repository<Material>
    {
        private readonly PersistenceContext _context;

        public MaterialRepository(PersistenceContext context) :
            base(context, context.Materiais)
        {
            _context = context;
        }

        public override List<Material> GetAll()
        {
            return _context.Materiais
                .Include(m => m.Acabamentos)
                .ToList();
        }

        public override Material GetById(long id)
        {
            return _context.Materiais
                .SingleOrDefault(m => m.Id == id);
        }

        public override bool Edit(long id, Material alterado)
        {
            Material encontrado = _context.Materiais.Find(id);

            if (encontrado == null)
            {
                return false;
            }

            encontrado.Descricao = alterado.Descricao;
            encontrado.Acabamentos = alterado.Acabamentos;

            _context.Materiais.Update(encontrado);
            _context.SaveChanges();

            return true;
        }
    }
}