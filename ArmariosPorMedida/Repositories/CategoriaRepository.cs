using System.Collections.Generic;
using System.Linq;
using ArmariosPorMedida.Model;
using Microsoft.EntityFrameworkCore;

namespace ArmariosPorMedida.Repositories
{
    public class CategoriaRepository : Repository<Categoria>
    {
        private readonly PersistenceContext _context;

        public CategoriaRepository(PersistenceContext context) :
            base(context, context.Categorias)
        {
            _context = context;
        }

        public override List<Categoria> GetAll()
        {
            return _context.Categorias
                .Include(c => c.SuperCat)
                .ToList();
        }    

        public override Categoria GetById(long id)
        {
            return _context.Categorias
                .Include(c => c.SuperCat)
                .Where(c => c.Id == id)
                .SingleOrDefault();
        }

        public override bool Edit(long id, Categoria categoriaAlterada)
        {
            Categoria categoriaEncontrada = _context.Categorias.Find(id);

            if (categoriaEncontrada == null)
            {
                return false;
            }

            categoriaEncontrada.Nome = categoriaAlterada.Nome;
            categoriaEncontrada.SuperCat = categoriaAlterada.SuperCat;

            _context.Categorias.Update(categoriaEncontrada);
            _context.SaveChanges();

            return true;
        }
    }
}