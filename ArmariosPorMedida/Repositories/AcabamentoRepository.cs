using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Repositories
{
    public class AcabamentoRepository : Repository<Acabamento>
    {
        private readonly PersistenceContext _context;

        public AcabamentoRepository(PersistenceContext context) : 
            base(context, context.Acabamentos)
        {
            _context = context;
        }

        public override bool Edit(long id, Acabamento alterado)
        {
            Acabamento encontrado = _context.Acabamentos.Find(id);

            if (encontrado == null)
            {
                return false;
            }

            encontrado.Descricao = alterado.Descricao;

            _context.Acabamentos.Update(encontrado);
            _context.SaveChanges();

            return true;
        }
    }
}