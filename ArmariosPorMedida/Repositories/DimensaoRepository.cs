using System;
using ArmariosPorMedida.Model;

namespace ArmariosPorMedida.Repositories
{

    public class DimensaoRepository : Repository<Dimensao>
    {
        private readonly PersistenceContext _context;

        public DimensaoRepository(PersistenceContext context) :
            base(context, context.Dimensoes)
        {
            _context = context;
        }
        
        public override bool Edit(long id, Dimensao alterada)
        {
            Dimensao encontrada = _context.Dimensoes.Find(id);

            if (encontrada == null)
            {
                return false;
            }

            if (alterada.GetType() == typeof(Discreta))
            {
                ((Discreta)encontrada).Altura = ((Discreta)alterada).Altura;
                ((Discreta)encontrada).Largura = ((Discreta)alterada).Largura;
                ((Discreta)encontrada).Profundidade = ((Discreta)alterada).Profundidade;
            }
            else if(alterada.GetType() == typeof(Continua))
            {
                ((Continua)encontrada).AlturaMinima = 
                    ((Continua)alterada).AlturaMinima;
                ((Continua)encontrada).AlturaMaxima = 
                    ((Continua)alterada).AlturaMaxima;
                
                ((Continua)encontrada).LarguraMinima = 
                    ((Continua)alterada).LarguraMinima;
                ((Continua)encontrada).LarguraMaxima = 
                    ((Continua)alterada).LarguraMaxima;
                
                ((Continua)encontrada).ProfundidadeMinima = 
                    ((Continua)alterada).ProfundidadeMinima;
                ((Continua)encontrada).ProfundidadeMaxima = 
                    ((Continua)alterada).ProfundidadeMaxima;
            }
            else
            {
                throw new ArgumentException("Dimension must be either range or discrete value");
            }

            _context.Dimensoes.Update(encontrada);
            _context.SaveChanges();

            return true;
        }
    }
}