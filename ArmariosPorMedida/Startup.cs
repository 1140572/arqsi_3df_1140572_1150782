﻿using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ArmariosPorMedida
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("Database");
            services.AddDbContext<PersistenceContext>(options => options.UseSqlServer(connection));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Adds Repositories
            services.AddScoped<IRepository<Produto>, ProdutoRepository>();
            services.AddScoped<IRepository<Categoria>, CategoriaRepository>();
            services.AddScoped<IRepository<Dimensao>, DimensaoRepository>();
            services.AddScoped<IRepository<Material>, MaterialRepository>();
            services.AddScoped<IRepository<Acabamento>, AcabamentoRepository>();

            // Adds Services
            services.AddScoped<IProdutoService, ProdutoServiceImpl>();
            services.AddScoped<ICategoriaService, CategoriaServiceImpl>();
            services.AddScoped<IDimensaoService, DimensaoServiceImpl>();
            services.AddScoped<IMaterialService, MaterialServiceImpl>();
            services.AddScoped<IAcabamentoService, AcabamentoServiceImpl>();

            // Object Mapper (DTO)
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Produto, ProdutoDTO>();
                cfg.CreateMap<Categoria, CategoriaDTO>();
                
                cfg.CreateMap<Dimensao, DimensaoDTO>()
                    .Include<Continua, ContinuaDto>()
                    .Include<Discreta, DiscretaDto>();
                cfg.CreateMap<Continua, ContinuaDto>();
                cfg.CreateMap<Discreta, DiscretaDto>();
                
                cfg.CreateMap<Material, MaterialDTO>();
                cfg.CreateMap<Acabamento, AcabamentoDTO>();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
