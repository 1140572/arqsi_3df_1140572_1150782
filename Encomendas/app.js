const express       = require('express'),
    request         = require('request'),
    app             = express(),
    mongoose        = require('mongoose'),
    bodyParser      = require('body-parser'),
    Encomendas      = require('./models/Encomenda'),
    ItensProduto    = require('./models/ItemProduto');

mongoose.connect("mongodb://sa:Password123@ds046037.mlab.com:46037/arqsi_encomendas");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const CORE_API_URL = "https://armarios-medida.azurewebsites.net/api/Produto/"

app.get("/", function(req, res) {
    res.redirect("/Encomenda");
})

app.get("/Encomenda", function(req, res) {
    Encomendas.find({}, function(error, encomendas) {
        if (error) {
            console.log(error);
            return;
        }

        res.send(encomendas);
    });
});

app.post("/Encomenda", function(req, res) {
    var novaEncomenda = {
        idCliente: req.body.idCliente,
        itens: req.body.itens,
        precoTotal: req.body.precoTotal
    }

    Encomendas.create(novaEncomenda, function(err) {
        if (err) {
            console.log(err);
            return;
        }

        res.redirect("/Encomenda");
    });
})

app.get("/Encomenda/:id", function(req, res) {
    Encomendas.findById(req.params.id).populate("itens").exec(function(err, encomendaEncontrada) {
        if (err) {
            console.log(err);
        } else {
            res.send(encomendaEncontrada);
        }
    });
});

app.get("/Encomenda/:id/Itens", function(req, res) {
    Encomendas.findById(req.params.id).populate("itens").exec(function(err, encomendaEncontrada) {
        if (err) {
            console.log(err);
        } else {
            res.send(encomendaEncontrada.itens);
        }
    });
});

app.put("/Encomenda/:id", function(req, res) {
    Encomendas.findById(req.params.id).populate("itens").exec(function(err, encomendaEncontrada) {
        if (err) {
            console.log(err);
        } else {
            encomendaEncontrada.idCliente = req.body.idCliente;
            encomendaEncontrada.itens = req.body.itens;
            encomendaEncontrada.precoTotal = precoTotal;

            encomendaEncontrada.save(function(err, encomendaAlterada) {
                if (err) {
                    return console.log(err);
                }

                res.redirect("Encomenda")
            });
        }
    });
});

app.delete("/Encomenda/:id", function(req, res) {
    Encomendas.findByIdAndRemove(req.body.id, function(err) {
        if (err) {
            console.log(err);
        }

        res.redirect("/Encomenda");
    })
});

app.get("/ItemDeProduto", function(req, res) {
    ItensProduto.find({}, function(error, itensProduto) {
        if (error) {
            console.log(error);
            return;
        }

        res.send(itensProduto);
    });
});

app.post("/ItemDeProduto", function(req, res) {
    let requestBody = req.body;
    let produtoEncontrado;

    // Faz uma GET Request à API Core (busca um produto com o id dado)
    request(CORE_API_URL + requestBody.idProduto, function (error, response, body) {
        if (error) {
            console.log(error);
            return;
        }

        if (response.statusCode == 404) {
            res.send("Não existe produto com o id fornecido. id = " + requestBody.idProduto);
            return;
        }

        produtoEncontrado = body;

        // Verificar se há discrepancia de atributos
        if (requestBody.nome != produtoEncontrado.nome
            || requestBody.preco != produtoEncontrado.preco
            || requestBody.categoria != produtoEncontrado.categoria) {
                res.send("ERRO: Discrepancia de atributos nos produtos");
                return;
        }

        // TODO Verificar Itens Filhos

        // Verificar Dimensoes
        const dimensaoPedida = requestBody.dimensao;
        const tipoDimensaoPedida = dimensaoPedida.type;

        if (tipoDimensaoPedida === "Continua" || tipoDimensaoPedida === "continua") {
            // TODO
        } else if (tipoDimensaoPedida === "Discreta" || tipoDimensaoPedida === "discreta") {
            // TODO
        } else {
            res.send("Erro a processar dimensao");
        }

        // Verificar Material
        if (!(requestBody.material === produtoEncontrado.material.type)) {
            res.send("Erro a processar material");
            return;
        } else {
            // TODO
        }



        ItensProduto.create(requestBody, function(err) {
            if (err) {
                console.log(err);
                return;
            }

            res.redirect("/Encomenda");
        });
    });
});

app.get("/ItemDeProduto/:id", function(req, res) {
    ItensProduto.findById(req.params.id, function(err, encontrado) {
        if (err) {
            console.log(err);
        } else {
            res.send(encontrado);
        }
    });
});

app.put("/ItemProduto/:id", function(req, res) {
    ItensProduto.findById(req.params.id, function(err, itemEncontrado) {
        if (err) {
            console.log(err);
        } else {
            itemEncontrado.idProduto = req.body.idProduto;

            itenEncontrado.save(function(err, itemAlterado) {
                if (err) {
                    return console.log(err);
                }

                res.redirect("/ItemProduto")
            });
        }
    });
});

app.delete("/ItemProduto/:id", function(req, res) {
    ItensProduto.findByIdAndRemove(req.body.id, function(err) {
        if (err) {
            console.log(err);
        }

        res.redirect("/ItemProduto");
    })
});

app.listen(8000, function() {
    console.log("Modulo Encomendas");
    console.log("Server Started");
});
