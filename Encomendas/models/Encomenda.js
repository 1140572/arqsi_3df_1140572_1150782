var mongoose        = require('mongoose');

var encomendaSchema = mongoose.Schema({
    idCliente: String,
    itens: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ItemProduto",
        autopopulate: true
    },
    precoTotal: Number
});

encomendaSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model("Encomenda", encomendaSchema);
