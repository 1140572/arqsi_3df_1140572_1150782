var mongoose        = require('mongoose');

var itemProdutoSchema = mongoose.Schema({
    idProduto: {
        type: Number,
        required: true
    },
    nome: {
        type: String,
        required: true,
    },
    preco: {
        type: Number,
        required: true,
    },
    categoria: {
        type: String,
        required: true,
    },
    itens: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "ItemProduto",
        autopopulate: true
    }],
    dimensao: {
        type: {
            type: String,
            required: true
        },
        altura: Number,
        largura: Number,
        profundidade: Number,
        alturaMinima: Number,
        alturaMaxima: Number,
        profundidadeMinima: Number,
        profundidadeMaxima: Number,
        larguraMinima: Number,
        larguraMaxima: Number
    },
    material: {
        type: String,
        required: true,
    },
    acabamento: {
        type: String,
        required: true,
    },
    data: {type: Date, default: Date.now}
});

itemProdutoSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model("ItemProduto", itemProdutoSchema);
