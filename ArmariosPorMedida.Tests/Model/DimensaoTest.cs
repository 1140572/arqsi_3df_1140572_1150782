using System;
using ArmariosPorMedida.Model;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Model
{
    public class DimensaoTest
    {
        [Test]
        public void TestDiscretaConstrutor()
        {
            new Discreta(5, 10, 20);

            Assert.Throws<ArgumentException>(() => new Discreta(-20, 5, -4));
        }

        [Test]
        public void TestCabeEmDiscreta()
        {
            Dimensao dim1 = new Discreta(10, 5, 10);
            Dimensao dim2 = new Discreta(5, 3, 5);
            Dimensao dim3 = new Discreta(10, 5, 10);
            Dimensao dim4 = new Discreta(20, 0, 5);
            
            Assert.That(dim2.CabeEm(dim1));
            Assert.That(!dim3.CabeEm(dim1));
            Assert.That(!dim4.CabeEm(dim1));
        }
        
        [Test]
        public void TestCabeEmContinua()
        {
            Dimensao dim1 = new Continua(
                10, 20,
                10, 20,
                10, 20);
            Dimensao dim2 = new Continua(
                12, 18,
                13, 15,
                17, 19);
            Dimensao dim3 = new Continua(
                10, 20,
                10, 20,
                10, 20);
            Dimensao dim4 = new Continua(
                5, 15,
                13, 19,
                15, 16);
            Dimensao dim5 = new Continua(
                11, 15,
                13, 25,
                15, 16);
            
            Assert.That(dim2.CabeEm(dim1));
            Assert.That(!dim3.CabeEm(dim1));
            Assert.That(!dim4.CabeEm(dim1));
            Assert.That(!dim5.CabeEm(dim1));
        }

        [Test]
        public void TestContinuaConstrutor()
        {
            new Continua(
                10, 20,
                10, 20,
                10, 20);

            Assert.Throws<ArgumentException>(() => new Continua(
                -20, 20,
                10, 20,
                10, 20)
            );
            
            Assert.Throws<ArgumentException>(() => new Continua(
                0, 40,
                50, 60,
                30, 0)
            );
        }
    }
}