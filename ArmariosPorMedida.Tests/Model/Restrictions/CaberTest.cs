using System.Collections.Generic;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Model.Restrictions;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Model
{
    public class CaberTest
    {
        private Restricao caber;
        private Categoria cat;
        private Produto filhoCabe;
        private Produto filhoCabe2;
        private Produto filhoNaoCabe;
        private Produto pai;
 
        [SetUp]
        public void Init()
        {
            caber = new Caber();
            cat = new Categoria("Cat01", null);
 
            List<Dimensao> l1 = new List<Dimensao>();
            l1.Add(new Discreta(5, 10, 5));
            pai = new Produto("TesteP1", 10.0, cat, null,
                l1, new List<Material>());
           
            List<Dimensao> l2 = new List<Dimensao>();
            l2.Add(new Discreta(3, 9, 4));
            filhoCabe = new Produto("TesteP2", 15.0, cat,
                l2, new List<Material>());
           
            List<Dimensao> l3 = new List<Dimensao>();
            l3.Add(new Discreta(3, 5, 2));
            filhoCabe2 = new Produto("TesteP3", 12.0, cat,
                l3, new List<Material>());
 
            List<Dimensao> l4 = new List<Dimensao>();
            l4.Add(new Discreta(10, 10, 5));
            filhoNaoCabe = new Produto("TesteP4", 23.7, cat,
                null, l4, new List<Material>());
        }
 
        [Test]
        public void testaProdutosCabem()
        {
            List<Produto> partes = new List<Produto>();
 
            caber.Run(pai, partes);
 
            partes.Add(filhoCabe);
            partes.Add(filhoCabe2);
 
            caber.Run(pai, partes);
        }
 
        [Test]
        public void testaProdutosNaoCabem()
        {
            List<Produto> partes = new List<Produto>();
 
            partes.Add(filhoCabe);
            partes.Add(filhoNaoCabe);
 
            Assert.Throws<RestricaoException>(() => caber.Run(pai, partes));
        }
    }
}