using System.Collections.Generic;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Model.Restrictions;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Model.Restrictions
{
    public class OcupacaoTest
    {
        [Test]
        public void TestaOcupacaoObrigatoria() {
            Produto pai = new Produto("Pai", 10.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto filho = new Produto("Filho", 5.0, null,
                new List<Dimensao>(), new List<Material>());

            Ocupacao restricao = new Ocupacao(filho);
 
            pai.Partes.Add(filho);
 
            restricao.Run(pai, pai.Partes);
        }
 
        [Test]
        public void TestaOcupacaoFilhoNaoParte() {
            Produto pai = new Produto("Pai", 10.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto filho = new Produto("Filho", 5.0, null,
                new List<Dimensao>(), new List<Material>());
 
            Ocupacao restricao = new Ocupacao(filho);
 
            pai.VerificaRestricoes(pai.Partes);
 
            Assert.Throws<RestricaoException>(() => restricao.Run(pai, pai.Partes));
        }
    }
}