using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using AutoMapper;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Model.DTO
{
    public class AutoMapper
    {
        [Test]
        public void testAutoMapperConfiguration()
        {
            Mapper.Initialize(cfg =>
           {
                cfg.CreateMap<Produto, ProdutoDTO>();
                cfg.CreateMap<Categoria, CategoriaDTO>();
                
                cfg.CreateMap<Dimensao, DimensaoDTO>()
                    .Include<Continua, ContinuaDto>()
                    .Include<Discreta, DiscretaDto>();
                cfg.CreateMap<Continua, ContinuaDto>();
                cfg.CreateMap<Discreta, DiscretaDto>();
                
                cfg.CreateMap<Material, MaterialDTO>();
                cfg.CreateMap<Acabamento, AcabamentoDTO>();
           });

            Mapper.Configuration.AssertConfigurationIsValid();
        }
    }
}