using System.Collections.Generic;
using ArmariosPorMedida.Model;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Model
{
    public class ProdutoTest
    {
        [Test]
        public void TestAddParte()
        {
            List<Dimensao> l1 = new List<Dimensao>();
            l1.Add(new Discreta(5, 10, 5));
            Produto p = new Produto("TesteP1", 10.0, null,
                                    l1, new List<Material>());

            Assert.That(p.Partes.Count == 0);

            // Add Valid Produto
            List<Dimensao> l2 = new List<Dimensao>();
            l2.Add(new Discreta(3, 9, 4));
            Produto pValid = new Produto("TesteP2", 15.0, null,
                                         l2, new List<Material>());

            p.AddParte(pValid);
            Assert.That(p.Partes.Count == 1);
            Assert.That(p.Partes.Contains(pValid));
        }
    }
}