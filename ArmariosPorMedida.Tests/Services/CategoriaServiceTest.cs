using System;
using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Moq;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Services
{
    [TestFixture]
    public class CategoriaServiceTest
    {
        private Mock<IRepository<Categoria>> _repository;

        private CategoriaServiceImpl _service;

        [OneTimeSetUp]
        public void Init()
        {
            try
            {
                Mapper.Configuration.AssertConfigurationIsValid();

            }
            catch (InvalidOperationException e)
            {
                Mapper.Initialize(cfg => { cfg.CreateMap<Categoria, CategoriaDTO>(); });
            }
        }

        [Test]
        public void GetAllTest()
        {
            List<Categoria> categorias = new List<Categoria>();

            Categoria c1 = new Categoria("Cat1", null);
            Categoria c2 = new Categoria("Cat2", null);

            c1.Id = 0;
            c2.Id = 1;

            categorias.Add(c1);
            categorias.Add(c2);

            CategoriaDTO c1DTO = Mapper.Map<Categoria, CategoriaDTO>(c1);
            CategoriaDTO c2DTO = Mapper.Map<Categoria, CategoriaDTO>(c2);

            List<CategoriaDTO> expected = new List<CategoriaDTO>();
            expected.Add(c1DTO);
            expected.Add(c2DTO);

            _repository = new Mock<IRepository<Categoria>>();
            _repository.Setup(repo => repo.GetAll()).Returns(categorias);

            _service = new CategoriaServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetAllCategorias()[0].Id, c1DTO.Id);
            Assert.AreEqual(_service.GetAllCategorias()[1].Id, c2DTO.Id);
            Assert.That(_service.GetAllCategorias().Count == 2);
        }

        [Test]
        public void GetByIdTest()
        {
            Categoria c1 = new Categoria("Cat1", null);

            CategoriaDTO c1DTO = Mapper.Map<Categoria, CategoriaDTO>(c1);

            _repository = new Mock<IRepository<Categoria>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(c1);

            _service = new CategoriaServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetCategoriaById(0).Id, c1DTO.Id);
        }

        [Test]
        public void AddTest()
        {
            Categoria cat = new Categoria("Categoria Teste", null);

            _repository = new Mock<IRepository<Categoria>>();
            _repository.Setup(repo => repo.Add(cat));

            _service = new CategoriaServiceImpl(_repository.Object);

            _service.AddCategoria(cat);

            _repository.Verify(repo => repo.Add(cat));
        }

        [Test]
        public void RemoveTest()
        {
            Categoria cat = new Categoria("Categoria Teste", null);

            _repository = new Mock<IRepository<Categoria>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(cat);
            _repository.Setup(repo => repo.Remove(0));

            _service = new CategoriaServiceImpl(_repository.Object);

            _service.RemoveCategoria(0);

            _repository.Verify(repo => repo.Remove(0));
        }
    }
}