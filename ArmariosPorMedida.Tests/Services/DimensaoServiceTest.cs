using System;
using System.Collections.Generic;
using ArmariosPorMedida.Controllers;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Moq;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Services
{
    public class DimensaoServiceTest
    {
        private Mock<IRepository<Dimensao>> _repository;

        private DimensaoServiceImpl _service;

        [OneTimeSetUp]
        public void Init()
        {
            try
            {
                Mapper.Configuration.AssertConfigurationIsValid();

            }
            catch (InvalidOperationException e)
            {
                Mapper.Initialize(cfg => { cfg.CreateMap<Dimensao, DimensaoDTO>(); });
            }
        }

        [Test]
        public void GetAllTest()
        {
            List<Dimensao> dimensoes = new List<Dimensao>();

            Dimensao dim1 = new Discreta(
                10, 5, 10);
            Dimensao dim2 = new Continua(
                10, 20,
                10, 20,
                10, 20);

            dim1.Id = 0;
            dim2.Id = 1;

            dimensoes.Add(dim1);
            dimensoes.Add(dim2);

            DimensaoDTO dim1DTO = Mapper.Map<Dimensao, DimensaoDTO>(dim1);
            DimensaoDTO dim2DTO = Mapper.Map<Dimensao, DimensaoDTO>(dim2);

            List<DimensaoDTO> expected = new List<DimensaoDTO>();
            expected.Add(dim1DTO);
            expected.Add(dim2DTO);

            _repository = new Mock<IRepository<Dimensao>>();
            _repository.Setup(repo => repo.GetAll()).Returns(dimensoes);

            _service = new DimensaoServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetAllDimensoes()[0].Id, dim1DTO.Id);
            Assert.AreEqual(_service.GetAllDimensoes()[1].Id, dim2DTO.Id);
            Assert.That(_service.GetAllDimensoes().Count == 2);
        }

        [Test]
        public void GetByIdTest()
        {
            Dimensao dim1 = new Discreta(
                10, 5, 10);
            
            DimensaoDTO dim1DTO = Mapper.Map<Dimensao, DimensaoDTO>(dim1);

            _repository = new Mock<IRepository<Dimensao>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(dim1);

            _service = new DimensaoServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetDimensaoById(0).Id, dim1DTO.Id);
        }

        [Test]
        public void AddTest()
        {
            // Modelos
            Dimensao dim1 = new Discreta(
                10, 5, 10);
            Dimensao dim2 = new Continua(
                10, 20,
                10, 20,
                10, 20);
            
            // Configuração POCOs
            DimensaoPOCO dimPoco1 = new DimensaoPOCO();
            dimPoco1.Type = "Discreta";
            dimPoco1.Altura = 10;
            dimPoco1.Profundidade = 5;
            dimPoco1.Largura = 10;
            
            DimensaoPOCO dimPoco2 = new DimensaoPOCO();
            dimPoco2.Type = "Continua";
            dimPoco2.AlturaMinima = 10;
            dimPoco2.AlturaMaxima = 20;
            dimPoco2.ProfundidadeMinima = 10;
            dimPoco2.ProfundidadeMaxima = 20;
            dimPoco2.LarguraMinima = 10;
            dimPoco2.LarguraMaxima = 20;
            
            DimensaoPOCO dimPocoInvalid = new DimensaoPOCO();
            dimPocoInvalid.Type = "Invalid";
            dimPocoInvalid.Altura = 7;
            dimPocoInvalid.Profundidade = 40;
            dimPocoInvalid.Largura = 20;

            _repository = new Mock<IRepository<Dimensao>>();
            _repository.Setup(repo => repo.Add(dim1));
            _repository.Setup(repo => repo.Add(dim2));
            
            _service = new DimensaoServiceImpl(_repository.Object);

            Assert.That(_service.AddDimensao(dimPoco1).Equals(dim1));
            Assert.That(_service.AddDimensao(dimPoco2).Equals(dim2));
            Assert.That(_service.AddDimensao(dimPocoInvalid) == null);

            _repository.Verify(repo => repo.Add(dim1));
            _repository.Verify(repo => repo.Add(dim2));
        }

        [Test]
        public void RemoveTest()
        {
            // Modelos
            Dimensao dim1 = new Discreta(
                10, 5, 10);

            // Mock Produto Repositorio
            _repository = new Mock<IRepository<Dimensao>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(dim1);
            _repository.Setup(repo => repo.Remove(0));

            // Initialize Produto Service
            _service = new DimensaoServiceImpl(_repository.Object);

            _service.RemoveDimensao(0);

            _repository.Verify(repo => repo.Remove(0));
        }
    }
}