using System;
using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Moq;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Services
{
    public class AcabamentoServiceTest
    {
        private Mock<IRepository<Acabamento>> _repository;

        private AcabamentoServiceImpl _service;

        [OneTimeSetUp]
        public void Init()
        {
            try
            {
                Mapper.Configuration.AssertConfigurationIsValid();

            }
            catch (InvalidOperationException e)
            {
                Mapper.Initialize(cfg => { cfg.CreateMap<Acabamento, AcabamentoDTO>(); });
            }
        }

        [Test]
        public void GetAllTest()
        {
            List<Acabamento> acabamentos = new List<Acabamento>();

            Acabamento acabamento1 =
                new Acabamento("Acabamento1");
            Acabamento acabamento2 =
                new Acabamento("Acabamento2");

            acabamento1.Id = 0;
            acabamento2.Id = 1;

            acabamentos.Add(acabamento1);
            acabamentos.Add(acabamento2);

            AcabamentoDTO acabamento1Dto =
                Mapper.Map<Acabamento, AcabamentoDTO>(acabamento1);
            AcabamentoDTO acabamento2Dto =
                Mapper.Map<Acabamento, AcabamentoDTO>(acabamento2);

            List<AcabamentoDTO> expected = new List<AcabamentoDTO>();
            expected.Add(acabamento1Dto);
            expected.Add(acabamento2Dto);

            _repository = new Mock<IRepository<Acabamento>>();
            _repository.Setup(repo => repo.GetAll()).Returns(acabamentos);

            _service = new AcabamentoServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetAllAcabamentos()[0].Id, acabamento1Dto.Id);
            Assert.AreEqual(_service.GetAllAcabamentos()[1].Id, acabamento2Dto.Id);
            Assert.That(_service.GetAllAcabamentos().Count == 2);
        }

        [Test]
        public void GetByIdTest()
        {
            Acabamento acabamento1 =
                new Acabamento("Acabamento1");

            AcabamentoDTO acabamento1Dto =
                Mapper.Map<Acabamento, AcabamentoDTO>(acabamento1);

            _repository = new Mock<IRepository<Acabamento>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(acabamento1);

            _service = new AcabamentoServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetAcabamentoById(0).Id, acabamento1Dto.Id);
        }

        [Test]
        public void AddTest()
        {
            Acabamento acabamento =
                new Acabamento("Acabamento");

            _repository = new Mock<IRepository<Acabamento>>();
            _repository.Setup(repo => repo.Add(acabamento));

            _service = new AcabamentoServiceImpl(_repository.Object);

            _service.AddAcabamento(acabamento);

            _repository.Verify(repo => repo.Add(acabamento));
        }

        [Test]
        public void RemoveTest()
        {
            Acabamento acabamento =
                new Acabamento("Acabamento");

            _repository = new Mock<IRepository<Acabamento>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(acabamento);
            _repository.Setup(repo => repo.Remove(0));

            _service = new AcabamentoServiceImpl(_repository.Object);

            _service.RemoveAcabamento(0);

            _repository.Verify(repo => repo.Remove(0));
        }
    }
}