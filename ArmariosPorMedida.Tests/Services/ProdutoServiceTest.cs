using System.Collections.Generic;
using ArmariosPorMedida.Controllers.POCO;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Moq;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Services
{
    [TestFixture]
    public class ProdutoServiceTest
    {
        private Mock<IRepository<Produto>> _repository;
        private Mock<IRepository<Categoria>> _catRepository;
        private Mock<IDimensaoService> _dimService;

        private ProdutoServiceImpl _produtoService;

        [Test]
        public void GetAllTest()
        {
            List<Produto> produtos = new List<Produto>();

            Produto p1 = new Produto("TesteP2", 10.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto p2 = new Produto("TesteP2", 15.0, null,
                new List<Dimensao>(), null);

            p1.Id = 0;
            p2.Id = 1;

            produtos.Add(p1);
            produtos.Add(p2);

            ProdutoDTO p1DTO = Mapper.Map<Produto, ProdutoDTO>(p1);
            ProdutoDTO p2DTO = Mapper.Map<Produto, ProdutoDTO>(p2);

            List<ProdutoDTO> expected = new List<ProdutoDTO>();
            expected.Add(p1DTO);
            expected.Add(p2DTO);

            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetAll()).Returns(produtos);

            _catRepository = new Mock<IRepository<Categoria>>();
            _dimService = new Mock<IDimensaoService>();

            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);

            Assert.AreEqual(_produtoService.GetAll()[0].Id, p1DTO.Id);
            Assert.AreEqual(_produtoService.GetAll()[1].Id, p2DTO.Id);
            Assert.That(_produtoService.GetAll().Count == 2);
        }

        [Test]
        public void GetByIdTest()
        {
            Produto p1 = new Produto("TesteP2", 10.0, null,
                new List<Dimensao>(), new List<Material>())
            {
                Id = 0
            };

            ProdutoDTO p1DTO = Mapper.Map<Produto, ProdutoDTO>(p1);

            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(p1);

            _catRepository = new Mock<IRepository<Categoria>>();
            _dimService = new Mock<IDimensaoService>();

            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);

            Assert.AreEqual(_produtoService.GetById(0).Id, p1DTO.Id);
        }

        [Test]
        public void GetPartesTest()
        {
            Produto p1 = new Produto("TesteP1", 10.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto p2 = new Produto("TesteP2", 5.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto p3 = new Produto("TesteP3", 15.0, null,
                new List<Dimensao>(), new List<Material>());

            p1.Id = 0;
            p2.Id = 1;
            p3.Id = 2;

            ProdutoDTO p2DTO = Mapper.Map<Produto, ProdutoDTO>(p2);
            ProdutoDTO p3DTO = Mapper.Map<Produto, ProdutoDTO>(p3);

            p1.Partes.Add(p2);
            p1.Partes.Add(p3);

            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(p1);

            _catRepository = new Mock<IRepository<Categoria>>();
            _dimService = new Mock<IDimensaoService>();

            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);

            List<ProdutoDTO> expected = new List<ProdutoDTO>();
            expected.Add(p2DTO);
            expected.Add(p3DTO);

            Assert.That(_produtoService.GetPartes(0).Count == 2);
            Assert.AreEqual(_produtoService.GetPartes(0)[0].Id, p2DTO.Id);
            Assert.AreEqual(_produtoService.GetPartes(0)[1].Id, p3DTO.Id);
        }

        [Test]
        public void AddTest()
        {
            // Categoria do produto expected
            Categoria cat = new Categoria("Categoria Teste", null);

            // Partes do produto expected
            List<Produto> partes = new List<Produto>();

            Produto p1 = new Produto("TesteP1", 10.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto p2 = new Produto("TesteP2", 5.0, null,
                new List<Dimensao>(), new List<Material>());
            Produto p3 = new Produto("TesteP3", 15.0, null,
                new List<Dimensao>(), new List<Material>());

            partes.Add(p1);
            partes.Add(p2);
            partes.Add(p3);

            // Produto Expected
            Produto expected = new Produto("Produto Expected", 15.0, cat,
                partes, new List<Dimensao>(), new List<Material>());

            List<long> idPartes = new List<long>();
            idPartes.Add(0);
            idPartes.Add(1);
            idPartes.Add(2);

            ProdutoPOCO poco = new ProdutoPOCO();
            poco.Nome = expected.Nome;
            poco.Preco = expected.Preco;
            poco.IdCategoria = 0;
            poco.Partes = idPartes;

            // Mock Produto Repositorio
            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(p1);
            _repository.Setup(repo => repo.GetById(1)).Returns(p2);
            _repository.Setup(repo => repo.GetById(2)).Returns(p3);

            // Mock Categoria Repositorio
            _catRepository = new Mock<IRepository<Categoria>>();
            _catRepository.Setup(repo => repo.GetById(0)).Returns(cat);

            // Mock Dimensao Service
            _dimService = new Mock<IDimensaoService>();

            // Initialize Produto Service
            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);

            Produto actual = _produtoService.Add(poco);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void EditTest()
        {
            Produto p = new Produto("TesteP1", 10.0, null,
                new List<Dimensao>(), new List<Material>());

            // Mock Produto Repositorio
            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(p);
            _repository.Setup(repo => repo.Edit(0, It.IsAny<Produto>()))
                .Returns(true);

            // Mock Categoria Repositorio
            _catRepository = new Mock<IRepository<Categoria>>();

            // Mock Dimensao Service
            _dimService = new Mock<IDimensaoService>();

            // Initialize Produto Service
            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);
        }

        [Test]
        public void RemoveTest()
        {
            Produto p = new Produto("TesteP1", 10.0, null,
                new List<Dimensao>(), new List<Material>());

            // Mock Produto Repositorio
            _repository = new Mock<IRepository<Produto>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(p);
            _repository.Setup(repo => repo.Remove(0));

            // Mock Categoria Repositorio
            _catRepository = new Mock<IRepository<Categoria>>();

            // Mock Dimensao Service
            _dimService = new Mock<IDimensaoService>();

            // Initialize Produto Service
            _produtoService = new ProdutoServiceImpl(_repository.Object,
                _catRepository.Object, _dimService.Object);

            _produtoService.Remove(0);

            _repository.Verify(repo => repo.Remove(0));
        }
    }
}