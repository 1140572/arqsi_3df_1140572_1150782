using System;
using System.Collections.Generic;
using ArmariosPorMedida.DTO;
using ArmariosPorMedida.Model;
using ArmariosPorMedida.Repositories;
using ArmariosPorMedida.Services;
using AutoMapper;
using Moq;
using NUnit.Framework;

namespace ArmariosPorMedida.Tests.Services
{
    public class MaterialServiceTest
    {
        private Mock<IRepository<Material>> _repository;

        private MaterialServiceImpl _service;

        [OneTimeSetUp]
        public void Init()
        {
            try
            {
                Mapper.Configuration.AssertConfigurationIsValid();

            }
            catch (InvalidOperationException e)
            {
                Mapper.Initialize(cfg => { cfg.CreateMap<Material, MaterialDTO>(); });
            }
        }

        [Test]
        public void GetAllTest()
        {
            List<Material> materiais = new List<Material>();

            Material mat1 = new Material("Material1", new List<Acabamento>());
            Material mat2 = new Material("Material1", new List<Acabamento>());

            mat1.Id = 0;
            mat2.Id = 1;

            materiais.Add(mat1);
            materiais.Add(mat2);

            MaterialDTO mat1Dto = Mapper.Map<Material, MaterialDTO>(mat1);
            MaterialDTO mat2Dto = Mapper.Map<Material, MaterialDTO>(mat2);

            List<MaterialDTO> expected = new List<MaterialDTO>();
            expected.Add(mat1Dto);
            expected.Add(mat2Dto);

            _repository = new Mock<IRepository<Material>>();
            _repository.Setup(repo => repo.GetAll()).Returns(materiais);

            _service = new MaterialServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetAllMateriais()[0].Id, mat1Dto.Id);
            Assert.AreEqual(_service.GetAllMateriais()[1].Id, mat2Dto.Id);
            Assert.That(_service.GetAllMateriais().Count == 2);
        }

        [Test]
        public void GetByIdTest()
        {
            Material mat1 = new Material("Material1", new List<Acabamento>());

            MaterialDTO mat1Dto = Mapper.Map<Material, MaterialDTO>(mat1);

            _repository = new Mock<IRepository<Material>>();
            _repository.Setup(repo => repo.GetById(0)).Returns(mat1);

            _service = new MaterialServiceImpl(_repository.Object);

            Assert.AreEqual(_service.GetMaterialById(0).Id, mat1Dto.Id);
        }

        [Test]
        public void AddTest()
        {
            Material mat = new Material("Material", new List<Acabamento>());

            _repository = new Mock<IRepository<Material>>();
            _repository.Setup(repo => repo.Add(mat));

            _service = new MaterialServiceImpl(_repository.Object);

            _service.AddMaterial(mat);

            _repository.Verify(repo => repo.Add(mat));
        }

        [Test]
        public void RemoveTest()
        {
            Material mat = new Material("Material", new List<Acabamento>());

            _repository = new Mock<IRepository<Material>>();
            _repository.Setup(repo => repo.GetById(0))
                .Returns(mat);
            _repository.Setup(repo => repo.Remove(0));

            _service = new MaterialServiceImpl(_repository.Object);

            _service.RemoveMaterial(0);

            _repository.Verify(repo => repo.Remove(0));
        }
    }
}